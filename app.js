
/*
npm i express --save
npm i express-hbs --save
npm i body-parser --save
npm i aes-js --save
npm i pbkdf2 --save
npm i sqlite3 --save or npm i mysql2 --save
npm i express-  validator --save
npm i express-session --save
npm i passport --save
npm i randomatic --save
npm i nodemailer --save
npm i connect-session-sequelize --save
npm i sequelize --save
npm i dotenv --save
npm i express-fileupload --save
npm i serve-favicon --save
*/


const express = require('express');
const app = express();

const hbs = require('express-hbs');
const bodyParser = require('body-parser');
const validator = require('express-validator');
const session = require('express-session');
const passport = require('passport');
const fileUpload = require('express-fileupload');
const logger = require(__dirname + '/modules/log').Logger;
const sequelizeStore = require('connect-session-sequelize')(session.Store);
const sequelize = require('sequelize');
const config = require(__dirname + '/modules/config');
const favicon = require('serve-favicon')

const port = config.port;

const router = require(__dirname + '/modules/route');

var mySequelize;

if(config.database === 'sqlite3') {
    // Database for sqlite3
    mySequelize = new sequelize(
        "",
        "",
        "", {
            "dialect": "sqlite",
            "storage": __dirname + "/" + config.db_name,
            "logging": false
    });
} else {
    //Database for MySQL
    mySequelize = new sequelize(
        config.db_database,
        config.db_user,
        config.db_password, {
            dialect: "mysql",
            host: config.db_host,
            logging: false
        }
    );
}

const myStore = new sequelizeStore({
    db: mySequelize});
    
app.use(favicon(__dirname + '/public/favicon.ico'));

app.use(session({
    secret: 'n5Z2nKfFo2W3UJGg4JbJ',
    resave: false,
    saveUninitialized: false,
    store: myStore,
    cookie: {maxAge: 3600000}
}));

app.use(passport.initialize());
app.use(passport.session());

myStore.sync();

app.engine('hbs', hbs.express4({
    partialsDir: __dirname + '/views/partials',
    defaultLayout: __dirname + '/views/index.hbs'
}));

app.set('view engine', 'hbs');
app.set('views', __dirname + '/views/layout');

app.use(fileUpload({
    useTempFiles : true,
    tempFileDir : __dirname+ '/tmp/',
    createParentPath: true,
    abortOnLimit: true
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(validator());

app.use('/', router);

app.use(express.static( __dirname + '/public/js'))      //for javascript files
app.use(express.static( __dirname + '/public/css'))     //for css files
app.use(express.static( __dirname + '/public/img'))     //for image files
app.use(express.static( __dirname + '/public/vendor'))  //for another library files

app.use((req, res, next) => {
    return res.status(404).render('404', {
        title: '404 Error',
        footer: false
    });
});

app.listen(port, () => {
    console.log(`App running! Listening on port ${port}!`);
    logger.info(`App running! Listening on port ${port}!`);
});