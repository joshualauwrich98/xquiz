const express = require('express');
const router = express.Router();

const main = require(__dirname + '/main').Main;
const logger = require(__dirname + '/log').Logger;

const path = require('path');
const url = require('url');    
const passport = require('passport');

const desc = "Learn by playing - Quizes - Beat the level - Be the top";

passport.serializeUser(function(user_id, done) {
    done(null, user_id);
});
  
passport.deserializeUser(function(user_id, done) {
      done(null, user_id);
});

function authenticationMiddleware () {  
	return (req, res, next) => {
	    if (req.isAuthenticated()) return next();
	    res.redirect('/auth');
	}
}

function adminCheck() {
    return (req, res, next) => {
        
    }
}

//GET
router.get('/', (req, res) => {
    res.type('html');
    res.render('loading', {
        title: 'Loading',
        description: desc,
        footer: false
    });
});

router.get('/home', (req, res) => {
    res.type('html');
    main.getAllLevels((result) => {
        if(result) {
            res.render('home', {
                title : 'Home',
                description: desc,
                footer: true,
                levels: result
            });
        } else {
            res.render('404', {
                title : '404 Error',
                description: desc,
                footer: false
            });
        }
    });
});

router.get('/profile', (req, res) => {
    main.getUserData(req.query.id, (result) => {
		main.getUserRank(result.data.username, (rank) => {
			res.type('html');
			res.render('profile', {
				title: 'Profile',
				footer: true,
				description: desc,
				user: result.data,
				statistic: result.statistic,
				level: result.level,
				highscore: main.getHighScore(),
				rank: rank
			});
		});
    });
});


router.get('/auth', (req, res) => {
    if(req.isAuthenticated()) {
        res.redirect('/dashboard');
    } else {
        res.type('html');
        res.render('auth', {
            title: 'Signin',
            footer: false
        });
    }
});

router.get('/confirmation', (req, res) => {
    let tokenId = req.query.token_id, tokenVal = req.query.token_val;
    if (tokenId && tokenVal) {
        main.completeRegistration(tokenId, tokenVal, (stat) => {
            if (stat && !stat.status) {
                console.log(stat.message);
                logger.error(stat.message);
                res.send(JSON.stringify({status:false, data: stat.message}));
            } else {
                res.type('html');
                res.render('auth', {
                    title: 'Account Confirmation',
                    type: {confirmation:true, registration:true},
                    footer: false
                });
            }
        });
    } else {
        if(req.isAuthenticated()) {
            res.redirect('/dashboard');
        } else {
            res.type('html');
            res.render('auth', {
                title: 'Account Confirmation',
                type: {confirmation:true},
                footer: false
            });
        }
    }
});

router.get('/dashboard', function(req, res) {
    if (req.isAuthenticated()) {
        main.getUserData(req.user, (data) => {
            if (data) {
                current_point_max = main.getMaxPoint(data.statistic, data.level);
                res.type('html');
                res.render('dashboard', {
                    title: 'Dashboard',
                    description: desc,
                    user: data.data,
                    statistic: data.statistic,
                    current_point: current_point_max,
                    footer: true
                });
            } else {
                res.redirect('/auth');
            }
        });
    } else {
        res.redirect('/auth');
    }
});

router.get('/reset-password', (req,res) => {
    let tokenId = req.query.token_id, tokenVal = req.query.token_val;
    if (tokenId && tokenVal) {
        main.getSecurityQuestion(tokenId, tokenVal, (result) => {
            if (result.status) {
                res.type('html');
                res.render('auth', {
                    title: 'Reset Password',
                    type: {resetPassword: true, security_question: result.security_question},
                    footer: false
                });
            } else {
                if(req.isAuthenticated()) {
                    res.redirect('/dashboard');
                } else {
                    res.redirect('/auth');
                }
            }
        });
    } else {
        if(req.isAuthenticated()) {
            res.redirect('/dashboard');
        } else {
            res.redirect('/auth');
        }
    }
});

router.get('/signout', (req,res) => {
    req.logout();
    res.redirect('/auth');
});

router.get('/levels', authenticationMiddleware(), (req, res) => {
    res.type('json');
    let id = req.query.id;
    main.getStatisticAndLevel(id, (data) => {
        if (data) {
            let start = req.query.start;
            let result = main.getLevelList(start, data.levels);
            res.send({status: true, data: result});
        } else {
            res.send({status: false});
        }
    });
});

//POST
router.post('/check/email', (req, res) => {
    main.checkEmail(req, (stat) => {
       res.send({status: stat});
       res.end();
    });
});

router.post('/check/username', (req, res) => {
    main.checkUsername(req, (stat) => {
       res.send({status: stat});
       res.end();
    });
});

router.post('/auth', (req, res) => {
    const error = main.validateSignIn(req);
    res.type('json');
    if (error) {
        console.log(`errors:  ${JSON.stringify(error)}`);
        logger.error(error);
        res.send(JSON.stringify({status:false, data: error}));
    } else {
        main.getUserIDFromLogin(req, (user_id) => {
            if (user_id) {
                req.login(user_id, (err) => {
                    res.send({status:true});
                });
            } else {
                res.send({status:false, data: {msg:'Email and password not match!'}});
            }
        });
    }
});

router.post('/confirmation', (req, res) => {
    let tokenId = req.body.token_id, tokenVal = req.body.token_val;
    main.completeRegistration(tokenId, tokenVal, (stat) => {
        if (stat && !stat.status) {
            console.log(stat.message);
            logger.error(stat.message);
            res.send(JSON.stringify({status:false, data: stat.message}));
        } else {
            res.send({status: true});
        }
    });
});

router.post('/reset-password', (req,res) => {
    let error = main.validateResetPassPassword(req);
    res.type('json');
    if (error) {
        console.log(`errors:  ${JSON.stringify(error)}`);
        logger.error(error);
        res.send(JSON.stringify({status:false, data: error}));
    } else {
        let password = req.body.forgot_pass_password, tokenId = req.body.token_id, tokenVal = req.body.token_val;
        let answer = req.body.reset_pass_security_ans;
        main.changePassword(tokenId, tokenVal, password, answer, (stat) => {
            if (stat && !stat.status) {
                console.log(stat.message);
                logger.error(stat.message);
                res.send(JSON.stringify({status:false, data: stat.message}));
            } else {
                res.send({status:true});
            }
        });
    }
});

router.post('/play', authenticationMiddleware(), (req, res) => {
    let question_id = req.body.question_id;
    let user_id = req.body.user_id;
    let current_level = req.body.current_level;
    let answers = req.body.answers;
    let minute = req.body.minute;
    res.type('json');
    main.getResult(question_id, user_id, current_level, answers, minute, (result) => {
        res.send(result);
    });
});


//PUT
router.put('/auth', (req, res) => {
    let error = main.validateSignUp(req);
    res.type('json');
    if (error) {
        console.log(`errors:  ${JSON.stringify(error)}`);
        logger.error(error);
        res.send(JSON.stringify({status:false, data: error}));
    } else {
        main.trySignUp(req, (result) => {
            if (result.error) {
                let err = new Object();
                err.msg = "There is error when parsing the input or the server is broken. Please contact us for further information."
                console.log(`errors:  ${JSON.stringify(error)}`);
                console.log(err);
                logger.error(error);
                res.send(JSON.stringify({status:false, data: err}));
            } else {
                res.send(JSON.stringify({status: true, token_id:result.token_id}));   
            }
        });
        
    }
});

router.put('/reset-password', (req, res) => {
    let error = main.validateResetPassEmail(req);
    res.type('json');
    if (error) {
        console.log(`errors:  ${JSON.stringify(error)}`);
        logger.error(error);
        res.send(JSON.stringify({status:false, data: error}));
    } else {
        main.sendRequestPasswordEmail(req, (result) => {
            if (!result.status) {
                let err = new Object();
                err.msg = "Email not found! Try entering your email address again."
                console.log(`errors:  ${JSON.stringify(error)}`);
                console.log(err);
                logger.error(error);
                res.send(JSON.stringify({status:false, data: err}));
            } else {
                res.send(JSON.stringify({status: true}));
            }
        })
    }
});

router.put('/play', authenticationMiddleware(), (req, res) => {
    res.type('json');
    let planet_id = req.body.level_id;
    main.getQuestion(planet_id, (result) => {
        if (result) {
            res.send(JSON.stringify(result));
        }
    });
});

router.put('/profile/:id', authenticationMiddleware(), (req, res) => {
    main.changePicture(req, (result) => {
        res.type('json');
        res.send(result);
    });
});


module.exports = router;