var fs = require('fs');
const path = require('path');

var Logger = module.exports.Logger = {};

let filePath = path.join(__dirname, '..', 'logs');
if (!fs.existsSync(filePath)){
  fs.mkdirSync(filePath);
}

filePath = path.join(__dirname, '..', 'logs', 'info.txt');
fs.writeFile(filePath, "", { flag: 'wx' }, function (err) {
  console.log("Created info.txt!");
});

filePath = path.join(__dirname, '..', 'logs', 'error.txt');
fs.writeFile(filePath, "", { flag: 'wx' }, function (err) {
  console.log("Created error.txt");
});

filePath = path.join(__dirname, '..', 'logs', 'debug.txt');
fs.writeFile(filePath, "", { flag: 'wx' }, function (err) {
  console.log("Created debug.txt");
});

filePath = path.join(__dirname, '..', 'logs', 'users.txt');
fs.writeFile(filePath, "", { flag: 'wx' }, function (err) {
  console.log("Created users.txt");
});

var infoStream = fs.createWriteStream(path.join(__dirname, '..', 'logs', 'info.txt'));
var errorStream = fs.createWriteStream(path.join(__dirname, '..', 'logs', 'error.txt'));
var debugStream = fs.createWriteStream(path.join(__dirname, '..', 'logs', 'debug.txt'));
var usersStream = fs.createWriteStream(path.join(__dirname, '..', 'logs', 'users.txt'));

Logger.info = function(msg) {
  var message = new Date().toISOString() + " : " + msg + "\n";
  infoStream.write(message);
};

Logger.debug = function(msg) {
  var message = new Date().toISOString() + " : " + msg + "\n";
  debugStream.write(message);
};

Logger.error = function(msg) {
  var message = new Date().toISOString() + " : " + msg + "\n";
  errorStream.write(message);
};

Logger.users = function(users) {
  var message = new Date().toISOString() + " : \n" + users + "\n";
  usersStream.write(message);
};