const aesjs = require('aes-js');
const pbkdf2 = require('pbkdf2');

const logger = require(__dirname + '/log').Logger;
const config = require(__dirname + '/config');

const password = 'sF4AYhK2401CbKZq5ohA';
const salt = 'n5Z2nKfFo2W3UJGg4JbJ';
const mode = 'sha512';
const counter = 2706;
const key = pbkdf2.pbkdf2Sync(password, salt, 1, 256 / 8, mode);

const nodemailer = require("nodemailer");

var transporter;

if (config.mode === 'production') {
	transporter = nodemailer.createTransport({
		host: config.smtp_host,
		port: config.smtp_port,
		secure: config.smtp_secure,
		auth: {
			user: config.smtp_user,
			pass: config.smtp_password
		}
	});

	transporter.verify(function(error, success) {
		if (error) {
		console.log(error);
		} else {
		console.log("Server is ready to take our messages");
		logger.info('Server is ready to take our messages');
		}
	});
}

var Util = module.exports.Util = {};

Util.encrypt = function (text) {

	let textBytes = aesjs.utils.utf8.toBytes(text);
	let count = new aesjs.ModeOfOperation.ctr(key, new aesjs.Counter(counter));
	let encryptedBytes = count.encrypt(textBytes);
	let encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);
	return encryptedHex;
}

Util.sendConfirmationMail = async function (content) {
	logger.info(`email: ${content.email}, username: ${content.username}, token_id: ${content.token_id}, token_val: ${content.token_val}`);
	let emailText = "Hi" + content.username + "! ";
	emailText += "You registered your email as an account in XQuiz. Confirm your email address to complete the registration" +
					`by inserting this code: ${content.token_val} in the confirmation account page. You also can clicking the button 
					below or go to this link: http://xquiz.online/confirmation?token_id=${content.token_id}&token_val=${content.token_val}`;
	let htmlText = `<div style="margin:auto;text-align: center; width: 80%">
					<h1 style="text-align: center">XQuiz</h1>
					<p>You registered your email as an account in XQuiz and this is your activation code:</p>
					<div style="margin: 5% auto; cursor:pointer" title="Click to copy">
						<span id="code"  onclick="copy();" style="padding:2%; border: 1px solid black; font-size: 25px; font-weight: bold">${content.token_val}</span>
					</div>
					<span id="toast" style="display:none; position:absolute; bottom: 10px; left: 10px; background-color:#e78080; font-size: 18px; padding: 10px">Coppied!</span>
					<script>
						function copy() {
							const el = document.createElement('textarea');
							el.value = document.getElementById('code').innerHTML;
							document.body.appendChild(el);
							el.select();
							document.execCommand('copy');
							document.body.removeChild(el);
							document.getElementById('toast').style.display = "";
							setTimeout(function(){ document.getElementById('toast').style.display="none"; }, 2000);
						}
					</script>
					<p>Confirm your email address to complete the registration
						by inserting above code in our confirmation page or clicking the button below. You can also manually 
						go to this link: <a href="http://xquiz.online/confirmation?token_id=${content.token_id}&token_val=${content.token_val}">
							http://xquiz.online/confirmation?token_id=${content.token_id}&token_val=${content.token_val}</a></p>
					<a href="http://xquiz.online/confirmation?token_id=${content.token_id}&token_val=${content.token_val}">
						<button style="cursor: pointer;background-color: #6675df; border: 2px black solid; border-radius: 5px; 
						padding: 2% 4%; color: white; margin:auto; text-align: center">Confirm account</button></a>
					<div style="margin-top: 2%; background-color: beige; padding: 2% 3%">
						<div>Jl. Jend Sudirman Gg. Talibin 48/75, 40184, Bandung, West Java, Indonesia</div>
						<div>If you did not register your email, please contact us at <a href="mailto:support@anchovy-studios.net">support@anchovy-studios.net</a></div>
					</div>
				</div>`;
	let mailOptions = {
		from: "Anchovy Studios <joshua_lau@anchovy-studios.net>",
		to: content.email,
		subject: "Confirm XQuiz Account",
		text: emailText,
		html: htmlText
	};
	logger.info(`Sending message... ${content.email}`);
	let info = await transporter.sendMail(mailOptions);
	
	console.log("Message sent: %s", info.messageId);
	logger.info(`Message sent: ${info.messageId}`);
}

Util.sendConfirmationSuccessMail = async function (content) {
	logger.info(`email: ${content.email}, username: ${content.username}`);
	let emailText = "Hi" + content.username + "! ";
	emailText += "Your account confirmation succeeded! You have successfully registered to our system. Thank you for joining us! If you have any question";
	emailText += "just send us an email at support@anchovys-studios.net. Hope you enjoy our apps and learn something for it.";
	let htmlText = `<div style="margin:auto;text-align: center; width: 80%">
						<h1 style="text-align: center">XQuiz</h1>
						<h3 style="color: #555555">Hi ${content.username}! Your account confirmation succeeded!</h3>
						<p>You have successfully registered to our system. Thank you for joining us! If you have any question 
							just send us an email at <a href="mailto:support@anchovy-studios.net">support@anchovy-studios.net</a></p>
						<p>Hope you enjoy our apps and learn something for it. 
						<div style="margin-top: 2%; background-color: beige; padding: 2% 3%">
							<div>Jl. Jend Sudirman Gg. Talibin 48/75, 40184, Bandung, West Java, Indonesia</div>
							<div>Contact Support: <a href="mailto:support@anchovy-studios.net">support@anchovy-studios.net</a></div>
						</div>
					</div>`;
	let mailOptions = {
		from: "Anchovy Studios <joshua_lau@anchovy-studios.net>",
		to: content.email,
		subject: "Account Confirmed Successfully",
		text: emailText,
		html: htmlText
	};
	logger.info(`Sending message... ${content.email}`);
	let info = await transporter.sendMail(mailOptions);
	
	console.log("Message sent: %s", info.messageId);
	logger.info(`Message sent: ${info.messageId}`);
}

Util.sendForgotPassRequestMail = async function (content) {
	logger.info(`email: ${content.email}, username: ${content.username}, token_id: ${content.token_id}, token_val: ${content.token_val}`);
	let emailText = "Hi" + content.username + "! ";
	emailText += "You have requested a password reset for your account. Please click the button below or manully go to this link ";
	emailText += "http://xquiz.online/reset-password?token_id="+content.token_id+"&token_val="+content.token_val;
	emailText += "If you did not make this request, you can ignore this email or contact us at support@anchovy-studios.net";
	let htmlText = `<div style="margin:auto;text-align: center; width: 80%">
					<h1 style="text-align: center">XQuiz</h1>
					<p>You have requested a password reset for your account. Please click the button below or manually go to this link:
					<a href="http://xquiz.online/reset-password?token_id=${content.token_id}&token_val=${content.token_val}">
							http://xquiz.online/reset-password?token_id=${content.token_id}&token_val=${content.token_val}</a></p>

					<a href="http://xquiz.online/reset-password?token_id=${content.token_id}&token_val=${content.token_val}">
						<button style="cursor: pointer;background-color: #6675df; border: 2px black solid; border-radius: 5px; 
						padding: 2% 4%; color: white; margin:auto; text-align: center">Reset password</button></a>

					<div style="margin-top: 2%; background-color: beige; padding: 2% 3%">
						<div>Jl. Jend Sudirman Gg. Talibin 48/75, 40184, Bandung, West Java, Indonesia</div>
						<div>If you did not make this request, you can ignore this email or contact us at <a href="mailto:support@anchovy-studios.net">support@anchovy-studios.net</a></div>
					</div>
				</div>`;
	let mailOptions = {
		from: "Anchovy Studios <joshua_lau@anchovy-studios.net>",
		to: content.email,
		subject: "Reset Password Request",
		text: emailText,
		html: htmlText
	};
	logger.info(`Sending message... ${content.email}`);
	let info = await transporter.sendMail(mailOptions);
	
	console.log("Message sent: %s", info.messageId);
	logger.info(`Message sent: ${info.messageId}`);
}

Util.shuffle = function(array) {
	for (let i = array.length - 1; i > 0; i--) {
		let j = Math.floor(Math.random() * (i + 1));
		[array[i], array[j]] = [array[j], array[i]];
	}
	return array;
}