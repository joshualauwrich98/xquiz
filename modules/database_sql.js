const mysql = require('mysql');
const config = require(__dirname + '/config');
const logger = require(__dirname + '/log').Logger;
const pool  = mysql.createPool({
  connectionLimit : 10,
  host            : config.db_host,
  user            : config.db_user,
  password        : config.db_password,
  database        : config.db_database
});
 
pool.query('SELECT 1 + 1 AS solution', function (error, results, fields) {
  if (error) throw error;
  console.log("Connected to database!");
  logger.info("Connected to database!");
});

/**
  * ==========================================================================================
  *	USERS TABLE : (PRIMARY KEY user_id)
  *		1. Email : used for login
  *		2. Password : used for login, encrypted using CTR operation
  *		3. Username : visible to others
  *		4. Picture : visible to others
  *		5. Security question : used to reset password
  *		6. Security answer : used to reset password
  *		7. Created at : time when this user created
  *		8. Last online : time when this user logged on
  *   9. Is admin : true if this is an admin
  *   10. Is activated : true if this user already activated
  *   11. Token id = stroring id for one way token
  *   12. Token val = storing value for one way token 
  * 
  * STATISTICS TABLE : (PRIMARY KEY statistic_id, FOREIGN KEY user_id -> USERS.id)
  *   1. User : user that have this
  *		2. Current level : for statistic, show current user level
  *		3. Games played : for statistic, show total games played from created
  *	  4. Current point : for statistic, show current point in current level
  *		5. Time played : for statistic, show total time in hour and minute play
  *		6. Total question : for statistic, show total question given to this user
  *		7. Total right answer : for statistic, show total right answer for question given
  *	
  *	QUESTIONS TABLE : (PRIMARY KEY question_id, FOREIGN KEY level -> LEVEL.id)
  *		1. Content : question content in html format
  *		2. Choices : multiple choices given to user in format A|B|C|D|right_answer
  *		3. Level : question level
  *   4. Last update : show when this question last updated
  *	
  *	LEVELS TABLE : (PRIMARY KEY level_id)
  *		1. Point : point to level up
  *		2. Name : level name
  *   3. Description : this level description
  *   4. Number : the level number
  * 
  * BADGES TABLE : (PRIMARY KEY badge_id)
  *   1. Img : badge image
  *   2. Name : badge name
  *   3. Description : show how to get this badge
  *	==========================================================================================
*/

var DB = module.exports.DB = {};

/**
 * Create the tables
 */
pool.query(`
    CREATE TABLE IF NOT EXISTS users (
        user_id INTEGER PRIMARY KEY AUTO_INCREMENT,
        email VARCHAR(256) NOT NULL UNIQUE,
        username VARCHAR(256) NOT NULL UNIQUE,
        password VARCHAR(256) NOT NULL,
	      picture VARCHAR(512) NOT NULL DEFAULT 'profile_picture/0/default-0.png',
	      security_question VARCHAR(512) NOT NULL,
        security_answer VARCHAR(256) NOT NULL,
        created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
        last_online DATETIME DEFAULT CURRENT_TIMESTAMP,
        is_admin BOOLEAN DEFAULT 0,
        is_activated BOOLEAN DEFAULT 0,
        token_id VARCHAR(256) NULL UNIQUE,
        token_val VARCHAR(8) NULL
    );
`, (error, results, fields) => {
  // if(error) throw error;
  console.log("CREATED TABLE users");
  logger.info("CREATED TABLE users");
  pool.query(`
    CREATE TABLE IF NOT EXISTS levels (
        level_id INTEGER PRIMARY KEY AUTO_INCREMENT,
        name VARCHAR(256) NOT NULL UNIQUE,
        point INTEGER NOT NULL,
        description VARCHAR(512) NOT NULL,
        number INTEGER NOT NULL
    );
  `, (error, results, fields) => {
    // if(error) throw error;
    console.log("CREATED TABLE levels");
    logger.info("CREATED TABLE levels");
    pool.query(`
      CREATE TABLE IF NOT EXISTS statistics (
          statistic_id INTEGER PRIMARY KEY AUTO_INCREMENT,
          user INTEGER,
          current_level INTEGER NOT NULL DEFAULT 1,
          games_played INTEGER NOT NULL DEFAULT 0,
          current_point INTEGER NOT NULL DEFAULT 0,
          time_played INTEGER NOT NULL DEFAULT 0,
          total_question INTEGER NOT NULL DEFAULT 0,
          total_right_answer INTEGER NOT NULL DEFAULT 0,
          FOREIGN KEY(user) REFERENCES users(user_id)
      );
    `, (error, results, fields) => {
      // if(error) throw error;
      console.log("CREATED TABLE statistics");
      logger.info("CREATED TABLE statistics");
      pool.query(`
        CREATE TABLE IF NOT EXISTS questions (
            question_id INTEGER PRIMARY KEY AUTO_INCREMENT,
            content TEXT NOT NULL,
            choices TEXT NOT NULL,
            last_update DATETIME DEFAULT CURRENT_TIMESTAMP,
            level INTEGER,
            FOREIGN KEY(level) REFERENCES levels(level_id)
        );
      `, (error, results, fields) => {
        // if(error) throw error;
        console.log("CREATED TABLE questions");
        logger.info("CREATED TABLE questions");
        pool.query(`
          CREATE TABLE IF NOT EXISTS badges (
              badge_id INTEGER PRIMARY KEY,
              name VARCHAR(256) NOT NULL UNIQUE,
              img VARCHAR(256) NOT NULL,
              description VARCHAR(512) NOT NULL
          );
        `, (error, results, fields) => {
          // if(error) throw error;
          console.log("CREATED TABLE badges");
          logger.info("CREATED TABLE badges");
          pool.query(`
          INSERT INTO levels ('level_id', 'name', 'point', 'description', 'number') VALUES
          (1, 'ZEANUS', 250, 'This is the first planet for your first mission', 1),
          (2, 'MUNGORTH', 1000, 'First battle of the game, huh?', 2),
          (3, 'DONZILARA', 2500, 'Planet full of monster. Can you defeat it?', 3);
    
          INSERT INTO questions(content, choices, level) VALUES('TITLE tag must be within ...','Title|Form|Header|Body|3',1);INSERT INTO questions(content, choices, level) VALUES('Text within EM tag is displayed as ...','Bold|Italic |List|Indented|2',1);INSERT INTO questions(content, choices, level) VALUES('Text within STRONG tag is displayed as ...','Bold|Italic|List|Indented|1',1);INSERT INTO questions(content, choices, level) VALUES('UL Tag is used to ...','Display the numbered list|Underline the text|Display the bulleted list|Bold the text|3',1);INSERT INTO questions(content, choices, level) VALUES('Which tag is used to display the numbered list?','OL|DL|UL|LI|1',1);INSERT INTO questions(content, choices, level) VALUES('Which tag is used to display the large font size?','LARGE|BIG|SIZE|FONT|2',1);INSERT INTO questions(content, choices, level) VALUES('SCRIPT tag can be placed within ...','Header|Body|Both Header & Body|None of the all|3',1);INSERT INTO questions(content, choices, level) VALUES('Using P tag will ...','Start a new paragraph|Break the line|End the current paragraph|None of the all|1',1);INSERT INTO questions(content, choices, level) VALUES('TD tag is used for ...','Table heading|Table Records|Table row|None of the above|4',1);INSERT INTO questions(content, choices, level) VALUES('Which is true to change the text color to red?','BODY BGCOLOR=RED|BODY TEXT=RED|BODY COLOR=RED|None of the all|3',3);INSERT INTO questions(content, choices, level) VALUES('WWW stands for ....','World Worm Web|World Wide Web|World Word Web|None of the above|2',2);INSERT INTO questions(content, choices, level) VALUES('Which of the following software is used to view web pages?','Web Browser|Internet Browser|Page Browser|All of the above|1',2);INSERT INTO questions(content, choices, level) VALUES('ISP stands for ...','International Service Provider|Internet Service Provider|Internet Service Presenter|None of the above|2',2);INSERT INTO questions(content, choices, level) VALUES('What does HTML stand for?','Hyperlinks and Text Markup Language|Hyper Text Markup Language|Home Tool Markup Language|Hyper Tool Markup Language|2',2);INSERT INTO questions(content, choices, level) VALUES('Who is making the Web standards?','The World Wide Web Consortium|Google|Microsoft|Mozilla|1',2);INSERT INTO questions(content, choices, level) VALUES('Choose the correct HTML element for the largest heading:','&lt;head&gt;|&lt;h6&gt;|&lt;heading&gt;|&lt;h1&gt;|4',1);INSERT INTO questions(content, choices, level) VALUES('What is the correct HTML element for inserting a line break?','&lt;break&gt;|&lt;br&gt;|&lt;lb&gt;|&lt;hr&gt;|2',1);INSERT INTO questions(content, choices, level) VALUES('What is the correct HTML for adding a background color?',' &lt;body style="background-color:yellow;"&gt;|&lt;body bg="yellow"&gt;|&lt;background&gt;yellow&lt;/background&gt;|&lt;body color="yellow"&gt;|1',3);INSERT INTO questions(content, choices, level) VALUES('Choose the correct HTML element to define important text:','&lt;b&gt;|&lt;important&gt;|&lt;i&gt;|&lt;strong&gt;|4',1);INSERT INTO questions(content, choices, level) VALUES('Choose the correct HTML element to define emphasized text','&lt;italic&gt;|&lt;em&gt;|&lt;i&gt;|&lt;emp&gt;|2',1);INSERT INTO questions(content, choices, level) VALUES('What is the correct HTML for creating a hyperlink?','&lt;a url="http://www.w3schools.com"&gt;W3Schools.com&lt;/a&gt;|&lt;a&gt;http://www.w3schools.com&lt;/a&gt;|&lt;a name="http://www.w3schools.com"&gt;W3Schools.com&lt;/a&gt;|&lt;a href="http://www.w3schools.com"&gt;W3Schools&lt;/a&gt;|4',2);INSERT INTO questions(content, choices, level) VALUES('Which character is used to indicate an end tag?','*|/|&lt;|^|2',1);INSERT INTO questions(content, choices, level) VALUES('How can you open a link in a new tab/browser window?','&lt;a href="url" new&gt;|&lt;a href="url" target="_blank"&gt;|&lt;a href="url" target="new"&gt;|&lt;a href="url" target="_newtab"&gt;|2',2);INSERT INTO questions(content, choices, level) VALUES('Which of these elements are all &lt;table&gt; elements?','&lt;table&gt;&lt;head&gt;&lt;tfoot&gt;|&lt;table&gt;&lt;tr&gt;&lt;tt&gt;|&lt;table&gt;&lt;tr&gt;&lt;td&gt;|&lt;thead&gt;&lt;body&gt;&lt;tr&gt;|3',1);INSERT INTO questions(content, choices, level) VALUES('What is the correct HTML for making a checkbox?','&lt;input type="checkbox"&gt;|&lt;check&gt;|&lt;checkbox&gt;|&lt;input type="check"&gt;|1',2);INSERT INTO questions(content, choices, level) VALUES('What is the correct HTML for making a text input field?','&lt;textfield&gt;|&lt;input type="textfield"&gt;|&lt;textinput type="text"&gt;|&lt;input type="text"&gt;|4',2);INSERT INTO questions(content, choices, level) VALUES('What is the correct HTML for making a drop-down list?','&lt;list&gt;|&lt;select&gt;|&lt;input type="list"&gt;|&lt;input type="dropdown"&gt;|2',2);INSERT INTO questions(content, choices, level) VALUES('What is the correct HTML for making a text area?','&lt;input type="textbox"&gt;|&lt;input type="textarea"&gt;|&lt;textarea&gt;|&lt;textbox&gt;|3',2);INSERT INTO questions(content, choices, level) VALUES('What is the correct HTML for inserting an image?','&lt;img src="image.gif" alt="MyImage"&gt;|&lt;img alt="MyImage"&gt;image.gif&lt;/img&gt;|&lt;image src="image.gif" alt="MyImage"&gt;|&lt;img href="image.gif" alt="MyImage"&gt;|1',2);INSERT INTO questions(content, choices, level) VALUES('What is the correct HTML for inserting a background image?','&lt;body style="background-image:url(background.gif)"&gt;|&lt;body bg="background.gif"&gt;|&lt;background img="background.gif"&gt;|&lt;body style="bg: url(background.gif)"&gt;|1',3);INSERT INTO questions(content, choices, level) VALUES('Which HTML element defines the title of a document?','&lt;title&gt;|&lt;meta&gt;|&lt;header&gt;|&lt;head&gt;|1',1);INSERT INTO questions(content, choices, level) VALUES('Which HTML attribute specifies an alternate text for an image, if the image cannot be displayed?','longdesc|title|alt|src|3',1);INSERT INTO questions(content, choices, level) VALUES('Which doctype is correct for HTML5?','&lt;!DOCTYPE HTML5&gt;|&lt;!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 5.0//EN" "http://www.w3.org/TR/html5/strict.dtd"&gt;|&lt;!DOCTYPE html&gt;|&lt;!DOCTYPE html5&gt;|3',2);INSERT INTO questions(content, choices, level) VALUES('Which HTML element is used to specify a footer for a document or section?','&lt;bottom&gt;|&lt;section&gt;|&lt;end&gt;|&lt;footer&gt;|4',1);INSERT INTO questions(content, choices, level) VALUES('What is the correct HTML element for playing video files?','&lt;movie&gt;|&lt;video&gt;|&lt;media&gt;|&lt;film&gt;|2',1);INSERT INTO questions(content, choices, level) VALUES('What is the correct HTML element for playing audio files?','&lt;audio&gt;|&lt;mp3&gt;|&lt;sound&gt;|&lt;media&gt;|1',1);INSERT INTO questions(content, choices, level) VALUES('The HTML global attribute, "contenteditable" is used to:','Return the position of the first found occurrence of content inside a string|Specifies a context menu for an element. The menu appears when a user right-clicks on the element|Update content from the server|Specify whether the content of an element should be editable or not|4',2);INSERT INTO questions(content, choices, level) VALUES('In HTML, onblur and onfocus are:','HTML Elements|Event attributes|Style attributes|CSS Specification|2',2);INSERT INTO questions(content, choices, level) VALUES('The HTML &lt;canvas&gt; element is used to:','create draggable elements|manipulate data in MySQL|draw graphics|display database records|3',1);INSERT INTO questions(content, choices, level) VALUES('In HTML, which attribute is used to specify that an input field must be filled out?','validate|required|formvalidate|placeholder|2',2)
          INSERT INTO questions(content, choices, level) VALUES('Which input type defines a slider control?','search|range|controls|slider|2',1);INSERT INTO questions(content, choices, level) VALUES('Which HTML element is used to display a scalar measurement within a range?','&lt;meter&gt;|&lt;measure&gt;|&lt;gauge&gt;|&lt;range&gt;|1',1);INSERT INTO questions(content, choices, level) VALUES('Which HTML element defines navigation links?','&lt;navigate&gt;|&lt;nav&gt;|&lt;navigation&gt;|&lt;navigations&gt;|2',1);INSERT INTO questions(content, choices, level) VALUES('In HTML, what does the &lt;aside&gt; element define?','Content aside from the page content|A navigation list to be shown at the left side of the page|The ASCII character-set; to send information between computers on the Internet|A bookmarks to jump from one section to another|1',1);INSERT INTO questions(content, choices, level) VALUES('Which HTML element is used to specify a header for a document or section?','&lt;header&gt;|&lt;head&gt;|&lt;top&gt;|&lt;section&gt;|1',1);INSERT INTO questions(content, choices, level) VALUES('What does CSS stand for?','Computer Style Sheets|Creative Style Sheets|Colorful Style Sheets|Cascading Style Sheets|4',3);INSERT INTO questions(content, choices, level) VALUES('What is the correct HTML for referring to an external style sheet?','&lt;style src="mystyle.css"&gt;|&lt;link rel="stylesheet" type="text/css" href="mystyle.css"&gt;|&lt;stylesheet&gt;mystyle.css&lt;/stylesheet&gt;|&lt;link src="mystyle.css" rel="stylesheet" type="text/css"|2',3);INSERT INTO questions(content, choices, level) VALUES('Where in an HTML document is the correct place to refer to an external style sheet?','In the &lt;head&gt; section|In the &lt;body&gt; section|At the end of the document|Anywhere, it doe not matter|1',3);INSERT INTO questions(content, choices, level) VALUES('Which HTML tag is used to define an internal style sheet?','&lt;css&gt;|&lt;style&gt;|&lt;script&gt;|&lt;link&gt;|2',3);INSERT INTO questions(content, choices, level) VALUES('Which HTML attribute is used to define inline styles?','styles|font|class|style|4',3);INSERT INTO questions(content, choices, level) VALUES('Which is the correct CSS syntax?','body:color=black;|{body;color:black;}|body {color: black;}|{body:color=black;}|4',3);INSERT INTO questions(content, choices, level) VALUES('How do you insert a comment in a CSS file?',' this is a comment|// this is a comment //|/* this is a comment */|// this is a comment|3',3);INSERT INTO questions(content, choices, level) VALUES('Which property is used to change the background color?','color|bgcolor|background-color|background|3',3);INSERT INTO questions(content, choices, level) VALUES('How do you add a background color for all &lt;h1&gt; elements?','h1 {background-color:#FFFFFF;}|h1.all {background-color:#FFFFFF;}|all.h1 {background-color:#FFFFFF;}|* h1 {background-color:#FFFFFF;}|1',3);INSERT INTO questions(content, choices, level) VALUES('Which CSS property is used to change the text color of an element?','fgcolor|color|text-color|font-color|2',3);INSERT INTO questions(content, choices, level) VALUES('Which CSS property controls the text size?','font-style|font-size |text-style|text-size|2',3);INSERT INTO questions(content, choices, level) VALUES('What is the correct CSS syntax for making all the &lt;p&gt; elements bold?','p {font-weight:bold;}|&lt;p style="font-size:bold;"&gt;|&lt;p style="text-size:bold;"&gt;|p {text-size:bold;}|1',3);INSERT INTO questions(content, choices, level) VALUES('How do you display hyperlinks without an underline?','a {text-decoration:none;}|a {text-decoration:no-underline;}|a {decoration:no-underline;}|a {underline:none;}|1',3);INSERT INTO questions(content, choices, level) VALUES('How do you make each word in a text start with a capital letter?','You can not do that with CSS|text-transform:capitalize|text-style:captialize|transform:capitalize|2',3);INSERT INTO questions(content, choices, level) VALUES('Which property is used to change the font of an element?','font-family|font-style|font-weight|font-text|1',3);INSERT INTO questions(content, choices, level) VALUES('How do you make the text bold?','style:bold;|font-weight:bold;|font:bold;|font-style:bold|2',3);INSERT INTO questions(content, choices, level) VALUES('How do you display a border like this:&lt;br&gt; The top border = 10 pixels&lt;br&gt; The bottom border = 5 pixels&lt;br&gt; The left border = 20 pixels&lt;br&gt; The right border = 1pixel?','border-width:10px 20px 5px 1px;|border-width:10px 1px 5px 20px;|border-width:10px 5px 20px 1px;|border-width:5px 20px 10px 1px;|2',3);INSERT INTO questions(content, choices, level) VALUES('Which property is used to change the left margin of an element?','indent|padding-left|margin-left|border-left|3',3);INSERT INTO questions(content, choices, level) VALUES('When using the padding property; are you allowed to use negative values?','No|Yes|Only &gt; -256|Only &gt; -1024|1',3);INSERT INTO questions(content, choices, level) VALUES('How do you make a list that lists its items with squares?','list: square;|list-type: square;|list-decoration:square|list-style-type: square; |4',3);INSERT INTO questions(content, choices, level) VALUES('How do you select an element with id "demo"?','demo|.demo|*demo|#demo|4',3);INSERT INTO questions(content, choices, level) VALUES('How do you select elements with class name "test"?','*test|.test|#test|test|2',3);INSERT INTO questions(content, choices, level) VALUES('How do you select all p elements inside a div element?','div p|div + p|div.p|p div|1',3);INSERT INTO questions(content, choices, level) VALUES('How do you group selectors?','Separate each selector with a plus sign|Separate each selector with a comma|Separate each selector with a space|Separate each selector with semicolon|2',3);INSERT INTO questions(content, choices, level) VALUES('What is the default value of the position property?','absolute|fixed|relative|static|4',3);INSERT INTO questions(content, choices, level) VALUES('Below is not html5 content model:','Phrasing content|Script content|Flow content|Interactive content|2',2);INSERT INTO questions(content, choices, level) VALUES('The attributes for &lt;img&gt; element is, except:','longdesc|vspace|description|usemap|3',2);INSERT INTO questions(content, choices, level) VALUES('Below is not html5 grouping elements:','side-nav|menu|article|main|1',2);INSERT INTO questions(content, choices, level) VALUES('Are CSS property names case-sensitive?','Yes|No|Only for two words property|Depends on browser|2',3);INSERT INTO questions(content, choices, level) VALUES('Does setting margin-top and margin-bottom have an affect on an inline element?','Yes|No|Only if the element have "float" property|Depends on browser|2',3)`
          , (error, results, fields) => {
            // if(error) throw error;
            console.log("CREATED TABLE badges");
            logger.info("CREATED TABLE badges");
          });
        });
      });
    });
  });
});

/**
 * Check if the given username already exists in database
 * @param username the checked username
 * @param cb the callback to the caller
 * @return true if the username already exists
 */
DB.checkUsernameExists = function(username, cb) {
  pool.query('SELECT * FROM users WHERE username=?', [username], (err1, rows1) => {
    if (!err1 && rows1.length > 0) {
      cb(true);
    } else {
      cb(false);
    }
  });
};

/**
 * Check if the given email already exists in database
 * @param email the checked email
 * @param cb the callback to the caller
 * @return true if the email already exists
 */
DB.checkEmailExists = function(email, cb) {
  pool.query('SELECT * FROM users WHERE email=?', [email], (err1, rows1) => {
    if (!err1 && rows1.length > 0) {
      cb(true);
    } else {
      cb(false);
    }
  })
};

/**
 * Create a new user into users table and statistics table
 */
DB.createUser = function(email, username, password, sec_ques, sec_ans, token_id, token_val, cb) {
  pool.query(`INSERT INTO users(email, username, password, security_question, security_answer, token_id, token_val)
            VALUES(?,?,?,?,?,?,?)`, [email, username, password, sec_ques, sec_ans, token_id, token_val], (err1, rows1) => {
              if (!err1) {
                pool.query(`INSERT INTO statistics(user) VALUES(?)`, [rows1.insertId], (err2, rows2) => {
                  if (!err2) {
                    cb({id: rows1.insertId});
                  } else {
                    console.log(err2);
                    logger.error(err2);
                    cb({error: err2});
                  }
                });
              } else {
                console.log(err1);
                logger.error(err1);
                cb({error: err1});
              }
            });
};

DB.isActivated = function(token_id, cb) {
  pool.query('SELECT * FROM users WHERE token_id=?', [token_id], (err1, rows1) => {
    if (!err1 && rows1.length > 0) {
      if (rows1.is_activated == 1) {
        cb(true);
        console.log("Already activated!");
        logger.error("Already activated!");
      } else {
        cb(false);
      }
    } else {
      console.log("There is no token matched with this one!");
      logger.error("There is no token matched with this one!");
      cb(true);
    }
  })
};

DB.completeRegistration = function (token_id, token_val, cb) {
  pool.query('SELECT * FROM users WHERE token_id=? AND token_val=?', [token_id, token_val], (err1, rows1) => {
    if (!err1) {
      if (rows1.length > 0) {
        pool.query('UPDATE users SET is_activated=1, token_val=NULL, created_at=NOW() WHERE token_id=? AND token_val=?', [token_id, token_val], (err2, rows2) => {
          if (!err2) {
            let temp = JSON.stringify(rows1[0]);
            temp = JSON.parse(temp);
            cb({status: true, message: temp});
          } else {
            console.log("Activation failed; database error! Contact our support centre for more information.");
            logger.error("Activation failed; database error! Contact our support centre for more information.");
            cb({status: false, message: "Activation failed; database error! Contact our support centre for more information."});
          }
        });
      } else {
        console.log("no id or token matched");
        logger.error("no id or token matched");
        cb({status: false, message: "There is no token matched with this token. Please check and try again to activate your account."});
      }
    } else {
      console.log("There is no token matched with this token. Please check and try again to activate your account.");
      logger.error("There is no token matched with this token. Please check and try again to activate your account.");
      cb({status: false, message: "There is no token matched with this token. Please check and try again to activate your account."});
    }
  });
};

DB.getUserIDLogin = function (email, password, cb) {
  pool.query('SELECT user_id FROM users WHERE email=? AND password=? AND is_activated=1', [email, password], (err1, rows1) => {
    if (!err1 && rows1.length > 0) {
      pool.query('UPDATE users SET last_online=NOW() WHERE user_id=?', [rows1[0].user_id], (err2, rows2) => {
        if (!err2) {
          cb(rows1[0].user_id);
        } else {
          console.log("Email and password not match!");
          logger.error("Email and password not match!");
          cb(null);
        }
      });
    } else {
      console.log("Email and password not match!");
      logger.error("Email and password not match!");
      cb(null);
    }
  })
};

DB.getUserData = function (user_id, cb) {
  pool.query('SELECT * FROM users WHERE user_id=?', [user_id], (err1, rows1) => {
    if (!err1 && rows1.length > 0) {
      pool.query('SELECT * FROM statistics WHERE user=?', [user_id], (err2, rows2) => {
        if (!err2 && rows2.length > 0) {
          let temp = JSON.stringify(rows2[0]);
          temp = JSON.parse(temp);
          pool.query('SELECT * FROM levels WHERE number<=?', [temp.current_level], (err3, rows3) => {
                if(!err3 && rows3.length > 0) {
                  pool.query('UPDATE users SET last_online=NOW() WHERE user_id=?', [user_id]);
                  cb({data: JSON.parse(JSON.stringify(rows1))[0], statistic: JSON.parse(JSON.stringify(rows2))[0], level: JSON.parse(JSON.stringify(rows3))});
                } else {
                  console.log("Can't select level!")
                  logger.error("Can't select level!");
                  cb(null);
                }
          });
        } else {
          console.log("Can't select user statistic!");
          logger.error("Can't select user statistic!");
          cb(null);
        }
      })
    } else {
      console.log("Can't select user!");
      logger.error("Can't select user!");
      cb(null);
    }
  })
};


DB.requestResetPassword = function (email, token_val, cb) {
  pool.query('SELECT * FROM users WHERE email=?', [email], (err1, rows1) => {
    if (!err1 && rows1.length > 0) {
      pool.query('UPDATE users SET token_val=? WHERE email=? AND token_id=?', [token_val, email, rows1[0].token_id], (err2, rows2) => {
        if (!err2) {
          cb({status: true, data: rows1[0]});
        } else {
          console.log("Can't find user!");
          logger.error("Can't find user!");
          cb({status: false, data: "Can't find user!"});
        }
      });
    } else {
      console.log("Can't find user!");
      logger.error("Can't find user!");
      cb({status: false, data: "Can't find user!"});
    }
  });
};

DB.changePassword = function (token_id, token_val, pass, answer, cb) {
  pool.query('UPDATE users SET token_val=NULL, password=? WHERE token_id=? AND token_val=? AND security_answer=?',
  [pass, token_id, token_val, answer], (err1, rows1) => {
    if (!err1 && rows1.changedRows > 0) {
      cb({status: true});
    } else {
      console.log("Can't find account!; " + err1);
      logger.error("Can't find account!; "+ err1);
      cb({status: false, message: "Can't find account; " + err1});
    }
  });
};

DB.getSecurityQuestion = function (token_id, token_val, cb) {
  pool.query('SELECT * FROM users WHERE token_id=? AND token_val=?', [token_id, token_val], (err1, rows1) => {
    if (!err1 && rows1.length > 0) {
      cb({status: true, security_question: rows1[0].security_question});
    } else {
      console.log("Can't find account!; " + err1);
      logger.error("Can't find account!; " + err1);
      cb({status: false, message: "Can't find account; " + err1});
    }
  });
};

DB.getStatisticAndLevel = function(id, cb) {
  pool.query('SELECT * FROM statistics WHERE user=?', [id], (err1, rows1) => {
    if (!err1 && rows1.length > 0) {
      let temp = JSON.stringify(rows1[0]);
      temp = JSON.parse(temp);
      pool.query('SELECT * FROM levels WHERE number<=? ORDER BY number ASC', [temp.current_level], (err2, rows2) => {
        if (!err2 && rows2) {
          let temp1 = JSON.stringify(rows2);
          temp1 = JSON.parse(temp1);
          cb({statistic: temp, levels: temp1});
        } else {
          console.log("Can't find data!; " + err2);
          logger.error("Can't find data!; "+ err2);
          cb(null);
        }
      });
    } else {
      console.log("Can't find data!; " +err1);
      logger.error("Can't find data!; "+err1);
      cb(null);
    }
  });
};

DB.getQuestions = function(level_id, cb) {
  pool.query('SELECT * FROM questions WHERE level=?', [level_id], (err1, rows1) => {
    if (!err1 && rows1.length > 0) {
      let temp = JSON.stringify(rows1);
      temp = JSON.parse(temp);
      cb(temp);
    } else {
      console.log("Can't find data!; " +err1);
      logger.error("Can't find data!; "+err1);
      cb(null);
    }
  });
};

DB.getAnswer = function(question_id, cb) {
  let statements = "";
  for(let i = 0; i < question_id.length; i++) {
    if (i != question_id.length-1) {
      statements = statements.concat(`question_id=${question_id[i]} OR `);
    } else {
      statements = statements.concat(`question_id=${question_id[i]}`);
    }
  }
  pool.query(`SELECT * FROM questions WHERE ${statements} ORDER BY question_id ASC`, (err1, rows1) => {
    if (!err1 && rows1.length > 0) {
      let temp = JSON.stringify(rows1);
      temp = JSON.parse(temp);
      cb({status: true, data: temp});
    } else {
      console.log("Can't find answer!; " +err1);
      logger.error("Can't find answer!; "+err1);
      cb({status: false, message: "Can't find answer; " + err1});
    }
  });
};

DB.updateStatistic = function(current_level, games_played, current_point, time_played,
  total_question, total_right_answer, user_id, cb) {
    pool.query(`UPDATE statistics SET current_level=?, games_played=?, current_point=?,
      time_played=?, total_question=?, total_right_answer=? WHERE user=?`,
      [current_level, games_played, current_point, time_played,
        total_question, total_right_answer, user_id], (err1, rows1) => {
          if (!err1 && rows1.changedRows > 0) {
            cb({status: true});
          } else {
            console.log("Can't find account!; " + err1);
            logger.error("Can't find account!; "+ err1);
            cb({status: false, message: "Can't find account; " + err1});
          }
    });
};

DB.getAllLevels = function(cb) {
  pool.query('SELECT * FROM levels', (err1, rows1) => {
    if (!err1 && rows1.length > 0) {
      let temp = JSON.stringify(rows1);
      temp = JSON.parse(temp);
      cb(temp);
    } else {
      console.log("Can't fetch levels!; " + err1);
      logger.error("Can't fetch levels!; "+ err1);
      cb(null);
    }
  });
};

DB.changePicture = function(path, id, cb) {
  pool.query('UPDATE users SET picture=? WHERE user_id=?', [path, id], (err1, rows1) => {
    if(!err1 && rows1.changedRows > 0) {
      cb(true);
    } else {
      console.log("Can't update profile picture!; " + err1);
      logger.error("Can't update profile picture!; "+ err1);
      cb(false);
    }
  });
};

DB.getHighScore = function(cb) {
  pool.query(`
  SELECT
    current_level, res, username, user_id
  FROM 
    ((SELECT (total_right_answer*100/total_question) as res, statistics.current_level, statistics.user
      FROM statistics) as temp 
      JOIN users ON temp.user = users.user_id)
    ORDER BY current_level DESC, res DESC`, (err1, rows1) => {
      if (!err1 && rows1.length > 0) {
        let temp = JSON.stringify(rows1);
        temp = JSON.parse(temp);
        cb(temp);
      } else {
        console.log("Can't get highscore!; " + err1);
        logger.error("Can't get highscore!; "+ err1);
        cb(null);
      }
  });
};

DB.getUserAdmin = function(id, cb) {
  pool.query('SELECT * FROM users WHERE id=? and is_admin=1', id, (err1, rows1) => {
    if (!err1 && rows1.length > 0) {
      let temp = JSON.stringify(rows1);
        temp = JSON.parse(temp);
        cb(temp);
      } else {
        console.log("Can't get admin!; " + err1);
        logger.error("Can't get admin!; "+ err1);
        cb(null);
      }
  });
};