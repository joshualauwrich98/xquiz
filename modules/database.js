const sqlite3 = require('sqlite3');
const path = require('path');
const config = require(__dirname + '/config');
const mydb = new sqlite3.Database(path.join(__dirname, '..', config.db_name), createDatabase);
const logger = require(__dirname + '/log').Logger;

/**
  * ==========================================================================================
  *	USERS TABLE : (PRIMARY KEY user_id)
  *		1. Email : used for login
  *		2. Password : used for login, encrypted using CTR operation
  *		3. Username : visible to others
  *		4. Picture : visible to others
  *		5. Security question : used to reset password
  *		6. Security answer : used to reset password
  *		7. Created at : time when this user created
  *		8. Last online : time when this user logged on
  *   9. Is admin : true if this is an admin
  *   10. Is activated : true if this user already activated
  *   11. Token id = stroring id for one way token
  *   12. Token val = storing value for one way token 
  * 
  * STATISTICS TABLE : (PRIMARY KEY statistic_id, FOREIGN KEY user_id -> USERS.id)
  *   1. User : user that have this
  *		2. Current level : for statistic, show current user level
  *		3. Games played : for statistic, show total games played from created
  *	  4. Current point : for statistic, show current point in current level
  *		5. Time played : for statistic, show total time in hour and minute play
  *		6. Total question : for statistic, show total question given to this user
  *		7. Total right answer : for statistic, show total right answer for question given
  *	
  *	QUESTIONS TABLE : (PRIMARY KEY question_id, FOREIGN KEY level -> LEVEL.id)
  *		1. Content : question content in html format
  *		2. Choices : multiple choices given to user in format A|B|C|D|right_answer
  *		3. Level : question level
  *   4. Last update : show when this question last updated
  *	
  *	LEVELS TABLE : (PRIMARY KEY level_id)
  *		1. Point : point to level up
  *		2. Name : level name
  *   3. Description : this level description
  *   4. Number : the level number
  * 
  * BADGES TABLE : (PRIMARY KEY badge_id)
  *   1. Img : badge image
  *   2. Name : badge name
  *   3. Description : show how to get this badge
  *	==========================================================================================
*/

var DB = module.exports.DB = {};

/**
 * Create the database
 */
function createDatabase(error) {
	if (!error) {
		mydb.serialize(function() {
      mydb.run(`
        CREATE TABLE IF NOT EXISTS users (
          user_id INTEGER PRIMARY KEY,
          email VARCHAR(256) NOT NULL UNIQUE,
          username VARCHAR(256) NOT NULL UNIQUE,
          password VARCHAR(256) NOT NULL,
          picture VARCHAR(512) NOT NULL DEFAULT 'profile_picture/0/default-0.png',
          security_question VARCHAR(512) NOT NULL,
          security_answer VARCHAR(256) NOT NULL,
          created_at DATETIME DEFAULT CURRENT_TIMESTAMP,
          last_online DATETIME DEFAULT CURRENT_TIMESTAMP,
          is_admin BOOL DEFAULT 0,
          is_activated BOOL DEFAULT 0,
          token_id VARCHAR(256) NULL UNIQUE,
          token_val VARCHAR(8) NULL
        );`);
      mydb.run(`
        CREATE TABLE IF NOT EXISTS statistics (
          statistic_id INTEGER PRIMARY KEY,
          user INTEGER,
          current_level INTEGER NOT NULL DEFAULT 1,
          games_played INTEGER NOT NULL DEFAULT 0,
          current_point INTEGER NOT NULL DEFAULT 0,
          time_played INTEGER NOT NULL DEFAULT 0,
          total_question INTEGER NOT NULL DEFAULT 0,
          total_right_answer INTEGER NOT NULL DEFAULT 0,
          FOREIGN KEY(user) REFERENCES users(user_id)
        );`)
	    mydb.run(`
        CREATE TABLE IF NOT EXISTS levels (
          level_id INTEGER PRIMARY KEY,
          name VARCHAR(256) NOT NULL UNIQUE,
          point INTEGER NOT NULL,
          description VARCHAR(512) NOT NULL,
          number INTEGER NOT NULL
        );`);
      mydb.run(`
        CREATE TABLE IF NOT EXISTS questions (
          question_id INTEGER PRIMARY KEY,
          content TEXT NOT NULL,
          choices TEXT NOT NULL,
          last_update DATETIME DEFAULT CURRENT_TIMESTAMP,
          level INTEGER,
          FOREIGN KEY(level) REFERENCES levels(level_id)
        );`);
      mydb.run(`
        CREATE TABLE IF NOT EXISTS badges (
          badge_id INTEGER PRIMARY KEY,
          name VARCHAR(256) NOT NULL UNIQUE,
          img VARCHAR(256) NOT NULL,
          description VARCHAR(512) NOT NULL
        );`);
      console.log("ALL TABLE CREATED!");
      logger.info("ALL TABLE CREATED!");
    });
  } else {
    console.log(error.message);
    logger.error(error.message);
  }
}

/**
 * Check if the given username already exists in database
 * @param username the checked username
 * @param cb the callback to the caller
 * @return true if the username already exists
 */
DB.checkUsernameExists = function(username, cb) {
  mydb.get('SELECT * FROM users WHERE username=?', [username], (err1, rows1) => {
    if (!err1 && rows1) {
      cb(true);
    } else {
      cb(false);
    }
  })
};

/**
 * Check if the given email already exists in database
 * @param email the checked email
 * @param cb the callback to the caller
 * @return true if the email already exists
 */
DB.checkEmailExists = function(email, cb) {
  mydb.get('SELECT * FROM users WHERE email=?', [email], (err1, rows1) => {
    if (!err1 && rows1) {
      cb(true);
    } else {
      cb(false);
    }
  })
};

DB.createUser = function(email, username, password, sec_ques, sec_ans, token_id, token_val, cb) {
  mydb.run(`INSERT INTO users(email, username, password, security_question, security_answer, token_id, token_val)
            VALUES(?,?,?,?,?,?,?)`, [email, username, password, sec_ques, sec_ans, token_id, token_val], (err1, rows1) => {
              if (!err1) {
                mydb.get('SELECT last_insert_rowid() as id', (err2, rows2) => {
                  if (!err2) {
                    mydb.run(`INSERT INTO statistics(user) VALUES(?)`, [rows2.id], (err3, rows3) => {
                      if (!err3) {
                        cb({id: rows2.id});
                      } else {
                        console.log(err3);
                        logger.error(err3);
                        cb({error: err3});
                      }
                    });
                  } else {
                    console.log(err2);
                    logger.error(err2);
                    cb({error: err2});
                  }
                });
              } else {
                console.log(err1);
                logger.error(err1);
                cb({error: err1});
              }
            });
};

DB.isActivated = function(token_id, cb) {
  mydb.get('SELECT * FROM users WHERE token_id=?', [token_id], (err1, rows1) => {
    if (!err1 && rows1) {
      if (rows1.is_activated == 1) {
        cb(true);
        console.log("Already activated!");
        logger.error("Already activated!");
      } else {
        cb(false);
      }
    } else {
      console.log("There is no token matched with this one!");
      logger.error("There is no token matched with this one!");
      cb(true);
    }
  })
};

DB.completeRegistration = function (token_id, token_val, cb) {
  mydb.get('SELECT * FROM users WHERE token_id=? AND token_val=?', [token_id, token_val], (err1, rows1) => {
    if (!err1) {
      if (rows1) {
        mydb.run("UPDATE users SET is_activated=1, token_val=NULL, created_at=strftime('%Y-%m-%d %H-%M-%S','now') WHERE token_id=? AND token_val=?", [token_id, token_val], (err2, rows2) => {
          if (!err2) {
            cb({status: true, message: rows1});
          } else {
            console.log("Activation failed; database error! Contact our support centre for more information.");
            logger.error("Activation failed; database error! Contact our support centre for more information.");
            cb({status: false, message: "Activation failed; database error! Contact our support centre for more information."});
          }
        });
      } else {
        console.log('no id or token matched');
        logger.error('no id or token matched');
        cb({status: false, message: "There is no token matched with this token. Please check and try again to activate your account."});
      }
    } else {
      console.log("There is no token matched with this token. Please check and try again to activate your account.");
      logger.error("There is no token matched with this token. Please check and try again to activate your account.");
      cb({status: false, message: "There is no token matched with this token. Please check and try again to activate your account."});
    }
  });
};

DB.getUserIDLogin = function (email, password, cb) {
  mydb.get('SELECT user_id FROM users WHERE email=? AND password=? AND is_activated=1', [email, password], (err1, rows1) => {
    if (!err1 && rows1) {
      mydb.run("UPDATE users SET last_online=strftime('%Y-%m-%d %H-%M-%S','now') WHERE user_id=?", [rows1.user_id], (err2, rows2) => {
        if (!err2) {
          cb(rows1.user_id);
        } else {
          console.log("Email and password not match!");
          logger.error("Email and password not match!");
          cb(null);
        }
      });
    } else {
      console.log("Email and password not match!");
      logger.error("Email and password not match!");
      cb(null);
    }
  })
};

DB.getUserData = function (user_id, cb) {
  mydb.get('SELECT * FROM users WHERE user_id=?', [user_id], (err1, rows1) => {
    if (!err1 && rows1) {
      mydb.get('SELECT * FROM statistics WHERE user=?', [user_id], (err2, rows2) => {
        if (!err2 && rows2) {
          mydb.all('SELECT * FROM levels WHERE number<=?', [rows2.current_level] , (err3, rows3) => {
            if (!err3 && rows3) {
              mydb.run("UPDATE users SET last_online=strftime('%Y-%m-%d %H-%M-%S','now') WHERE user_id=?", [user_id]);
              cb({data: rows1, statistic: rows2, level: rows3});
            } else {
              console.log("Can't select level!")
              logger.error("Can't select level!");
              cb(null);
            }
          });
        } else {
          console.log("Can't select user statistic!");
          logger.error("Can't select user statistic!");
          cb(null);
        }
      })
    } else {
      console.log("Can't select user!");
      logger.error("Can't select user!");
      cb(null);
    }
  })
};

DB.requestResetPassword = function (email, token_val, cb) {
  mydb.get('SELECT * FROM users WHERE email=?', [email], (err1, rows1) => {
    if (!err1 && rows1) {
      mydb.run('UPDATE users SET token_val=? WHERE email=? AND token_id=?', [token_val, email, rows1.token_id], (err2, rows2) => {
        if (!err2) {
          cb({status: true, data: rows1});
        } else {
          console.log("Can't find user!");
          logger.error("Can't find user!");
          cb({status: false, message: "Can't find user!"});
        }
      });
    } else {
      console.log("Can't find user!");
      logger.error("Can't find user!");
      cb({status: false, message: "Can't find user!"});
    }
  });
};

DB.changePassword = function (token_id, token_val, pass, answer, cb) {
  mydb.serialize(function() {
    mydb.run('UPDATE users SET token_val=NULL, password=? WHERE token_id=? AND token_val=? AND security_answer=?',
    [pass, token_id, token_val, answer], (err1, rows1) => {
      mydb.get('SELECT changes() as res', (err2, rows2) => {
        if (!err1 && !err2 && rows2.res > 0) {
          cb({status: true});
        } else {
          console.log("Can't find account!; " + err1);
          logger.error("Can't find account!; "+ err1);
          cb({status: false, message: "Can't find account; " + err1});
        }
      });
    });
  });
};

DB.getSecurityQuestion = function (token_id, token_val, cb) {
  mydb.get('SELECT * FROM users WHERE token_id=? AND token_val=?', [token_id, token_val], (err1, rows1) => {
    if (!err1 && rows1) {
      cb({status: true, security_question: rows1.security_question});
    } else {
      console.log("Can't find account!; " + err1);
      logger.error("Can't find account!; " + err1);
      cb({status: false, message: "Can't find account; " + err1});
    }
  });
};

DB.getStatisticAndLevel = function(id, cb) {
  mydb.get('SELECT * FROM statistics WHERE user=?', [id], (err1, rows1) => {
    if (!err1 && rows1) {
      mydb.all('SELECT * FROM levels WHERE number<=? ORDER BY number ASC', [rows1.current_level], (err2, rows2) => {
        if (!err2 && rows2) {
          cb({statistic: rows1, levels: rows2});
        } else {
          console.log("Can't find data!; " + err2);
          logger.error("Can't find data!; "+ err2);
          cb(null);
        }
      });
    } else {
      console.log("Can't find data!; " +err1);
      logger.error("Can't find data!; "+err1);
      cb(null);
    }
  });
};

DB.getQuestions = function(level_id, cb) {
  mydb.all('SELECT * FROM questions WHERE level=?', [level_id], (err1, rows1) => {
    if(!err1 && rows1) {
      cb(rows1);
    } else {
      console.log("Can't find data!; " +err1);
      logger.error("Can't find data!; "+err1);
      cb(null);
    }
  });
};

DB.getAnswer = function(question_id, cb) {
  let statements = "";
  for(let i = 0; i < question_id.length; i++) {
    if (i != question_id.length-1) {
      statements = statements.concat(`question_id=${question_id[i]} OR `);
    } else {
      statements = statements.concat(`question_id=${question_id[i]}`);
    }
  }
  mydb.all(`SELECT * FROM questions WHERE ${statements} ORDER BY question_id ASC`, (err1, rows1) => {
    if (!err1 && rows1) {
      cb({status: true, data: rows1});
    } else {
      console.log("Can't find answer!; " +err1);
      logger.error("Can't find answer!; "+err1);
      cb({status: false, message: "Can't find answer; " + err1});
    }
  });
};

DB.updateStatistic = function(current_level, games_played, current_point, time_played,
  total_question, total_right_answer, user_id, cb) {
    mydb.run(`UPDATE statistics SET current_level=?, games_played=?, current_point=?,
      time_played=?, total_question=?, total_right_answer=? WHERE user=?`,
      [current_level, games_played, current_point, time_played,
        total_question, total_right_answer, user_id], (err1, rows1) => {
          mydb.get('SELECT changes() as res', (err2, rows2) => {
            if (!err1 && !err2 && rows2.res > 0) {
              cb({status: true});
            } else {
              console.log("Can't find account!; " + err1);
              logger.error("Can't find account!; "+ err1);
              cb({status: false, message: "Can't find account; " + err1});
            }
          });
    });
};

DB.getAllLevels = function(cb) {
  mydb.all('SELECT * FROM levels', (err1, rows1) => {
    if (!err1 && rows1) {
      cb(rows1);
    } else {
      console.log("Can't fetch levels!; " + err1);
      logger.error("Can't fetch levels!; "+ err1);
      cb(null);
    }
  });
};

DB.changePicture = function(path, id, cb) {
  mydb.run('UPDATE users SET picture=? WHERE user_id=?', [path, id], (err1, rows1) => {
    mydb.get('SELECT changes() as res', (err2, rows2) => {
      if (!err1 && !err2 && rows2.res > 0) {
        cb(true);
      } else {
        console.log("Can't update profile picture!; " + err1);
        logger.error("Can't update profile picture!; "+ err1);
        cb(false);
      }
    });
  });
};

DB.getHighScore = function(cb) {
  mydb.all(`
  SELECT
    current_level, res, username, user_id
  FROM 
    ((SELECT (total_right_answer*100/total_question) as res, statistics.current_level, statistics.user
      FROM statistics) as temp 
      JOIN users ON temp.user = users.user_id)
    ORDER BY current_level DESC, res DESC`, (err1, rows1) => {
      if (!err1 && rows1) {
        cb(rows1);
      } else {
        console.log("Can't get highscore!; " + err1);
        logger.error("Can't get highscore!; "+ err1);
        cb(null);
      }
  });
};

DB.getUserAdmin = function(id, cb) {
  mydb.get('SELECT * FROM users WHERE id=? and is_admin=1', id, (err1, rows1) => {
    if (!err1 && rows1) {
      cb(temp);
    } else {
      console.log("Can't get admin!; " + err1);
      logger.error("Can't get admin!; "+ err1);
      cb(null);
    }
  });
};