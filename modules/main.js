const util = require(__dirname + '/utilities').Util;
const validator = require('express-validator');
const randomizer = require('randomatic');
const logger = require(__dirname + '/log').Logger;
const config = require(__dirname + '/config');
var db;

if(config.database === 'sqlite3') {
    db = require(__dirname + '/database').DB;
} else {
    db = require(__dirname + '/database_sql').DB;
}

var max_level = 1;
var max_point = 1;
db.getAllLevels((result) => {
    if (result) {
        max_level = result.length;
        max_point = result[result.length-1].point;
    }
});

var users = null;
db.getHighScore((result) => {
    if(result) users = result;
});

setInterval(function() {
    db.getHighScore((result) => {
        if(result) users = result;
        logger.users(users);
    });
}, 300000);

var Main = module.exports.Main = {};

/*
    email, password
*/
Main.validateSignIn = function (req) {
    req.checkBody('email', 'Email field cannot be empty.').notEmpty();
    req.checkBody('email', 'Email field must be between 5-100 characters long.').len(5,100);
    req.checkBody('email', 'The email you entered is invalid, please try again.').isEmail();
    req.checkBody('password', 'Password must include one lowercase character, one uppercase character, and a number.').matches(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/, "i");
    req.checkBody('password', 'Password field must be between 8-25 characters long.').len(8,25);
    
    return req.validationErrors();
};

/* 
    email, username, password, sequrity_question, security_answer
*/
Main.validateSignUp = function (req) {
    req.checkBody('email_signup', 'Email field cannot be empty.').notEmpty();
    req.checkBody('email_signup', 'Email field must be between 5-100 characters long.').len(5,100);
    req.checkBody('email_signup', 'The email you entered is invalid, please try again.').isEmail();
    req.checkBody('username', 'Username field cannot be empty.').notEmpty();
    req.checkBody('username', 'Username field must be between 8-25 characters long.').len(8,25);
    req.checkBody('password_signup', 'Password must include one lowercase character, one uppercase character, and a number.').matches(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/, "i");
    req.checkBody('password_signup', 'Password field must be between 8-25 characters long.').len(8,25);
    req.checkBody('re_password_signup', 'Password confirmation field must be the same as password field.').equals(req.body.password_signup);
    req.checkBody('security_question', 'Security question field cannot be empty.').notEmpty();
    req.checkBody('security_answer', 'Security answer field cannot be empty.').notEmpty();

    return req.validationErrors();
};

/*
    email
*/
Main.validateResetPassEmail = function (req) {
    req.checkBody('forgot_pass_email', 'Email field cannot be empty.').notEmpty();
    req.checkBody('forgot_pass_email', 'Email field must be between 5-100 characters long.').len(5,100);
    req.checkBody('forgot_pass_email', 'The email you entered is invalid, please try again.').isEmail();

    return req.validationErrors();
};

/*
    password
*/
Main.validateResetPassPassword = function (req) {
    req.checkBody('forgot_pass_password', 'Password must include one lowercase character, one uppercase character, and a number.').matches(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/, "i");
    req.checkBody('forgot_pass_password', 'Password field must be between 8-25 characters long.').len(8,25);
    req.checkBody('reset_pass_security_ans', 'You must fill this field.').notEmpty();
    return req.validationErrors();
};

/*
    Try create user to database
*/
Main.trySignUp = function(req, cb) {
    const content = req.body;
    var token_id = randomizer('Aa0', 30);
    var token_val = randomizer('0', 6);
    db.createUser(content.email_signup, content.username, util.encrypt(content.password_signup), content.security_question, content.security_answer, token_id, token_val,
        (id) => {
            if (id.id) {
                let mailContent = new Object();
                mailContent.email = content.email_signup;
                mailContent.username = content.username;
                mailContent.token_id = token_id;
                mailContent.token_val = token_val;
                if (config.mode === 'production') {
                    util.sendConfirmationMail(mailContent).catch(console.error);
                }
                console.log(`ID: ${id.id}, Token ID: ${token_id}, Token VAL: ${token_val}`);
                logger.info(`ID: ${id.id}, Token ID: ${token_id}, Token VAL: ${token_val}`);
                cb({error:false, token_id:token_id});
            } else {
                cb({error:true});
            }
        }
    );
};

/*
    Check if email already taken
*/
Main.checkEmail = function(req, cb) {
    db.checkEmailExists(req.body.email_signup, (status) => {
        if(status) cb(false);
        else cb(true);
    });
};

/*
    Check if username already taken
*/
Main.checkUsername = function(req, cb) {
    db.checkUsernameExists(req.body.username, (status) => {
        if(status) cb(false);
        else cb(true);
    });
};

/*
    Complete the registration
*/
Main.completeRegistration = function (token_id, token_val, cb) {
    db.isActivated(token_id, (result) => {
        if (result) {
            cb({status:false, message:"Your account has already activated!"});
        } else {
            db.completeRegistration(token_id, token_val, (stat) => {
                if (stat.status) {
                    let mailContent = new Object();
                    mailContent.email = stat.message.email;
                    mailContent.username = stat.message.username;
                    if (config.mode === 'production') {
                        util.sendConfirmationSuccessMail(mailContent).catch(console.error);
                    }
                    cb(stat);
                } else {
                    cb({status:false, message: "Error in database!"});
                }
            });
        }
    })
};

Main.getUserIDFromLogin = function(req, cb) {
    let pass = util.encrypt(req.body.password);
    db.getUserIDLogin(req.body.email, pass, (res) => {
       cb(res);
    });
};

Main.getUserData = function(id, cb) {
    db.getUserData(id, (data) => {
        cb(data);
    });
};

Main.sendRequestPasswordEmail = function (req, cb) {
    var token_val = randomizer('0', 6);
    db.requestResetPassword(req.body.forgot_pass_email, token_val, (result) => {
        if (result.status) {
            let mailContent = new Object();
            mailContent.email = result.data.email;
            mailContent.username = result.data.username;
            mailContent.token_id = result.data.token_id;
            mailContent.token_val = token_val;
            console.log(`Token id: ${result.data.token_id}, token val: ${token_val}`);
            if (config.mode === 'production') {
                util.sendForgotPassRequestMail(mailContent).catch(console.error);
            }
        }
        cb(result);
    });
};

Main.getSecurityQuestion = function(token_id, token_val, cb) {
    db.getSecurityQuestion(token_id, token_val, (result) => {
        cb(result);
    });
};

Main.changePassword = function (token_id, token_val, pass, answer, cb) {
    db.changePassword(token_id, token_val, util.encrypt(pass), answer, (result) => {
        cb(result);
    });
};

Main.getMaxPoint = function (statistic, level) {
    let curr = statistic.current_level;
    let res = 1;
    level.forEach(element => {
        if (element.number == curr) {
            res = element.point;
        }
    });
    return res;
};

Main.getLevelList = function (start, level) {
    let res = [];
    for (let i = start, j = 1; i < level.length; i++) {
        res.push(level[i]);
        j++;
        if (j > 5) break;
    }
    return res;
};

Main.getStatisticAndLevel = function(id, cb) {
    db.getStatisticAndLevel(id, (data) => {
        cb(data);
    });
};

Main.getQuestion = function(level_id, cb) {
    db.getQuestions(level_id, (result) => {
        if (result) {
            cb(util.shuffle(result).slice(0,10));
        } else {
            cb(null);
        }
    });
};

Main.getAllLevels = function(cb) {
    db.getAllLevels((result) => {
        cb(result);
    });
}

Main.getResult = function(question_id, user_id, current_level, answers, minute, cb) {
    db.getAnswer(question_id, (result) => {
        if(result.status) {
            answers.sort(function(a,b) {
                return a.q_id - b.q_id;
            });
            question_id.sort((a, b) => a - b);
            let total_right = 0;
            let temp;
            let wrong_answer = [];
            for(let i = 0; i < question_id.length; i++) {
                temp = result.data[i].choices.split('|');
                if (answers[i].val == temp[4]) {
                    total_right++;
                } else {
                    wrong_answer.push(result.data[i]);
                }
            }
            // console.log(question_id);
            // console.log(result.data);
            // console.log(wrong_answer);
            // console.log(answers);
            let final_score;
            if(result.data[0].level < current_level) 
                final_score = Math.round(total_right*((Math.pow(current_level,(5/3)))/(current_level-result.data[0].level)));
            else 
                final_score = Math.round(total_right*(10 + (Math.pow(current_level,(5/3)))));
            db.getStatisticAndLevel(user_id, (result2) => {
                if (result2) {
                    let stat = result2.statistic;
                    let leveled_up = false, level_cap = false;;
                    let max_score = result2.levels[(parseInt(current_level)-1)].point;
                    if(stat.current_point + final_score >= max_score) leveled_up = true;
                    let r_curr_lvl, r_games, r_curr_point, r_time_played, r_total_q, r_total_r;
                    if(leveled_up) {
                        r_curr_lvl = current_level + 1;
                        r_curr_point = (stat.current_point + final_score)%max_score;
                    } else {
                        r_curr_lvl = current_level;
                        r_curr_point = stat.current_point + final_score;
                    }
                    r_games = stat.games_played + 1;
                    r_time_played = stat.time_played + (10-minute);
                    r_total_q = stat.total_question + 10;
                    r_total_r = stat.total_right_answer + total_right;

                    if(r_curr_lvl > max_level) {
                        r_curr_lvl = max_level;
                        final_score = 0;
                        r_curr_point = max_point;
                        level_cap = true;
                    }

                    db.updateStatistic(r_curr_lvl, r_games, r_curr_point, r_time_played,
                        r_total_q, r_total_r, user_id, (result3) => {
                            if(result3) {
                                cb({
                                    status: true,
                                    total_score: final_score,
                                    leveled_up: leveled_up,
                                    total_right: total_right,
                                    wrong_answer: wrong_answer,
                                    level_cap: level_cap
                                });
                            } else {
                                cb({status: false, message: "Error when updating the statistic!"});
                            }
                    });
                } else {
                    cb({status: false, message: "Error when accessing the statistic!"});
                }
            });
        } else {
            cb(result);
        }
    });
};

Main.changePicture = function (req, cb) {
    if (Object.keys(req.files).length == 0) {
        cb({status: false, message: "No files were uploaded!"});
    }

    let file = req.files.picture;
    let path = `/profile_picture/${req.params.id}/pic.png`;

    file.mv(__dirname+'/../public/img'+path, function(err) {
        if (err)
            cb({status: false, message: "Error in server!"});
    
        
        db.changePicture(path, req.params.id, (result) => {
            if(result) {
                cb({status: true, message: "Successfully changed!"})
            } else {
                cb({status: false, message: "Error in database!"});
            }
        });
      });

};

Main.getHighScore = function() {
    res = users.slice(0,20);
    let i = 1;
    res.forEach((e) => {
        e.no = i;
        i++; 
    });
    return res;
};

Main.getUserRank = function(username, cb) {
    users.find(function(element, index, array) {
        if (element.username === username) {
            cb((parseInt(index)+1));
        }
    });
};

Main.getUserAdmin = function(id, cb) {
    db.getUserAdmin(id, (result) => {
        cb(result);
    });
}