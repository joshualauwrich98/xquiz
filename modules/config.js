require('dotenv').config();

/**
 * Store all the environtment variables data
 * (.env file required -> see readme.md)
 */
module.exports = {
  database: process.env.DATABASE,
  db_host: process.env.DB_HOST,
  db_database: process.env.DB_DATABASE,
  db_user: process.env.DB_USER,
  db_password: process.env.DB_PASSWORD,
  db_name: process.env.DB_NAME,
  port: process.env.PORT,
  mode: process.env.MODE,
  smtp_host: process.env.SMTP_HOST,
  smtp_port: process.env.SMTP_PORT,
  smtp_secure: process.env.SMTP_SECURE,
  smtp_user: process.env.SMTP_USER,
  smtp_password: process.env.SMTP_PASSWORD
};