function createRequest(method, url, response_type, parameter, cb) {
    let req = new XMLHttpRequest();
    req.open(method, url);
    req.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    req.responseType = response_type;
    req.onload = function(evt) {
        if ( req.status == 200 ) {
            cb(req.response);
        }
        else {
            cb(null);
            console.log('err', req );
        }
    }
    if(parameter) {
        req.send(JSON.stringify( parameter ));
    } else {
        req.send();
    }
}

/**
 * CUSTOM ELEMENT
 * This class is used to represent element that can be show and hide
 */
class ShowHideElement extends HTMLElement {
    constructor() {
        super();
        $(this).speed = $(this).attr('anim-speed');
    }

    showElement() {
        let parent = this.parentNode;
        let childs = parent.querySelectorAll('show-hide');
        let counter = 0;
        let element = this;
        function finish() {
            setTimeout(() => {
                $(element).hide().fadeIn(element.speed, () => {
                    $(element).css('visibility', 'visible');
                });
            }, 500);
            
        };
        childs.forEach((item,i,array) => {
            item.hideElement();
            counter++;
            if(counter === array.length) finish();
        });
        
        
    };

    hideElement() {
        $(this).fadeOut(this.speed);
    };
    
};

class SquareLoader extends HTMLElement {
    constructor() {
        super();
        const shadow = this.attachShadow({mode: 'open'});
        const container = document.createElement('div');
        container.classList.add('loader-cont');
        const link_for_css = document.createElement('link');
        link_for_css.rel = "stylesheet";
        link_for_css.href = "square-loader.css";
        let square_div;

        for(let i = 0; i < 9; i++) {
            square_div = document.createElement('div');
            square_div.classList.add('loader');
            container.appendChild(square_div);
        }
        
        shadow.appendChild(container);
        shadow.appendChild(link_for_css);
    }
}

customElements.define('show-hide', ShowHideElement);
customElements.define('square-loader', SquareLoader);
// Create a class for the element
class PopUpInfo extends HTMLElement {
    constructor() {
      // Always call super first in constructor
      super();
  
      // Create a shadow root
      const shadow = this.attachShadow({mode: 'open'});
  
      // Create spans
      const wrapper = document.createElement('span');
      wrapper.setAttribute('class', 'wrapper');
  
      const icon = document.createElement('span');
      icon.setAttribute('class', 'icon');
      icon.setAttribute('tabindex', 0);
  
      const info = document.createElement('span');
      info.setAttribute('class', 'info');
  
      // Take attribute content and put it inside the info span
      const text = this.getAttribute('data-text');
      info.textContent = text;
  
      // Insert icon
      let imgUrl;
      if(this.hasAttribute('img')) {
        imgUrl = this.getAttribute('img');
      } else {
        imgUrl = 'img/default.png';
      }
  
      const img = document.createElement('img');
      img.src = imgUrl;
      icon.appendChild(img);
  
      // Create some CSS to apply to the shadow dom
      const style = document.createElement('style');
      console.log(style.isConnected);
  
      style.textContent = `
        .wrapper {
          position: relative;
        }
        .info {
          font-size: 0.8rem;
          width: 200px;
          display: inline-block;
          border: 1px solid black;
          padding: 10px;
          background: white;
          border-radius: 10px;
          opacity: 0;
          transition: 0.6s all;
          position: absolute;
          bottom: 20px;
          left: 10px;
          z-index: 3;
        }
        img {
          width: 1.2rem;
        }
        .icon:hover + .info, .icon:focus + .info {
          opacity: 1;
        }
      `;
  
      // Attach the created elements to the shadow dom
      shadow.appendChild(style);
      console.log(style.isConnected);
      shadow.appendChild(wrapper);
      wrapper.appendChild(icon);
      wrapper.appendChild(info);
    }
  }
  
  // Define the new element
  customElements.define('popup-info', PopUpInfo);
/**
 * JQUERY UTILITIES
 * Restricts input for each element in the set to only accept numeric value.
 */
jQuery.fn.forceNumeric = function() {
    return this.each(function() {
        $(this).keydown(function(e) {
            var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
    });
};

! function(e) {
    function toRadian(e) {
        return e * Math.PI / 180
    }

    function t(t, a, s) {
        var o = e("#" + s + "canvas")[0],
            n = e("#" + s + "canvas"),
            i = o.getContext("2d"),
            l = o.width / 2,
            p = o.height / 2;
        i.beginPath(), i.arc(l, p, e(n).attr("data-radius"), -toRadian(90), -toRadian(90) + toRadian(t / 100 * 360), !1), i.fillStyle = "transparent", i.fill(), i.lineWidth = e(n).attr("data-width"), i.strokeStyle = e(n).attr("data-stroke"), i.stroke(), i.closePath(), "true" == e(n).attr("data-text").toLocaleLowerCase() && e("#" + s + " .clProg").val(a + ("true" == e(n).attr("data-percent").toLocaleLowerCase() ? "%" : ""))
    }
    e.fn.circularloader = function(r) {
        function a() {
            h.beginPath(), h.arc(u, f, i, 0, 2 * Math.PI, !1), h.fillStyle = n.backgroundColor, h.fill(), h.lineWidth = l, h.strokeStyle = n.progressBarBackground, h.stroke(), h.closePath(), d > 0 && t(d, p, o)
        }
        var s = this[0],
            o = s.id;
        if (0 == e("#" + o + " canvas").length) {
            var n = e.extend({
                    backgroundColor: "#ffffff",
                    fontColor: "#000000",
                    fontSize: "40px",
                    radius: 70,
                    progressBarBackground: "#cdcdcd",
                    progressBarColor: "#aaaaaa",
                    progressBarWidth: 25,
                    progressPercent: 0,
                    progressValue: 0,
                    showText: !0,
                    title: ""
                }, r),
                i = parseInt(n.radius),
                l = parseInt(n.progressBarWidth),
                p = parseInt(n.progressValue) > 0 ? parseInt(n.progressValue) : parseInt(n.progressPercent),
                d = parseInt(n.progressPercent),
                c = "color:" + n.fontColor + ";font-size:" + parseInt(n.fontSize) + "px;width:" + 2 * (i + l) + "px;vertical-align:middle;position:relative;background-color:transparent;border:0 none;transform:translateY(-48%);-webkit-transform: translateY(-48%);-ms-transform: translateY(-48%);height:" + 2 * (i + l) + "px;margin-left:-" + 2 * (i + l) + "px;text-align:center;padding:0;" + (n.showText ? "" : "display:none;");
            e('<canvas data-width="' + l + '" data-radius="' + i + '" data-stroke="' + n.progressBarColor + '" data-text=' + n.showText + " data-percent=" + (void 0 == r.progressValue ? !0 : !1) + ' id="' + o + 'canvas" width=' + 2 * (i + l) + " height=" + 2 * (i + l) + "></canvas>").appendTo(s), e('<input class="clProg" style="' + c + '" value="' + p + (void 0 == r.progressValue ? "%" : "") + '"></input>').appendTo(s), "" == n.title ? e("#" + o).css("height", 2 * (i + l)) : (e("#" + o).css("height", 2 * (i + l) + 20), e("#" + o + "canvas").before("<div class='titleCircularLoader' style='height:19px;text-align:center;'>" + n.title + "</div>"), e(".titleCircularLoader").css("width", 2 * (i + l)));
            var g = e("#" + o + "canvas")[0],
                h = g.getContext("2d"),
                u = g.width / 2,
                f = g.height / 2;
            e("#" + o + "canvas").offset().left, e("#" + o + "canvas").offset().top;
            a()
        } else if (void 0 != r.progressPercent || void 0 != r.progressValue) {
            var d = 0,
                p = 0;
            d = void 0 == r.progressPercent ? parseInt(r.progressValue) > 100 ? 100 : parseInt(r.progressValue) : parseInt(r.progressPercent) > 100 ? 100 : parseInt(r.progressPercent), p = void 0 == r.progressValue ? parseInt(r.progressPercent) > 100 ? 100 : parseInt(r.progressPercent) : parseInt(r.progressValue), t(d, p, o)
        }
        return this
    }
}(jQuery);

$.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null) {
       return null;
    }
    return decodeURI(results[1]) || 0;
}



/**
 * TEST
 */


 
  $(document).ready(function() {
    $(document).scroll(function () {
        var $nav = $(".navbar");
        $nav.toggleClass('bg-dark', $(this).scrollTop() > $nav.height()+157);
      });
  });