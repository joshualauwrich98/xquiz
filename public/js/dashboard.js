var curr_page = 1;
var prev_selected = null;
var curr_question = 1;
var curr_question_btn = null;
var question_id_list = [];
var timer_interval = null;

function updateCurrQuestion(new_question) {
    curr_question = new_question;
    document.querySelector(`#q${curr_question}`).showElement();
    updateActiveQuestionButton();
    submitButtonCheck();
}

function updateActiveQuestionButton() {
    let new_btn = $(`#qn-${curr_question}`);
    if (curr_question_btn) {
        if(curr_question_btn == new_btn) return;
        else curr_question_btn.removeClass('question-active-btn');
    }
    curr_question_btn = new_btn;
    curr_question_btn.addClass('question-active-btn');
}

function submitButtonCheck() {
    if (curr_question == 10) {
        $('#submit-btn').css('display', '');
    } else {
        $('#submit-btn').css('display', 'none');
    }
}

function nextButton() {
    if (curr_question == 10) return;
    else {
        updateCurrQuestion((parseInt(curr_question)+1));
    }
}

function prevButton() {
    if (curr_question == 1) return;
    else {
        updateCurrQuestion((parseInt(curr_question)-1));
    }
}

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    timer_interval = setInterval(function () {
                        minutes = parseInt(timer / 60, 10)
                        seconds = parseInt(timer % 60, 10);

                        minutes = minutes < 10 ? "0" + minutes : minutes;
                        seconds = seconds < 10 ? "0" + seconds : seconds;

                        display.textContent = minutes + ":" + seconds;

                        if (--timer < 0) {
                            clearInterval(timer_interval);
                            endGame();
                        }
                    }, 1000);
}

function endGame() {
    let minute = parseInt($('#timer').html());
    let answers = [];
    for(let i = 1; i <= 10; i++) {
        let temp = $(`#q${i} .answer-active-btn`);
        if (temp.length > 0) {
            answers.push({q_id: parseInt(temp.attr('q_id')), val: parseInt(temp.attr('idx'))});
        } else {
            temp = $(`#q${i} button`);
            answers.push({q_id: parseInt(temp.attr('q_id')), val: -1});
        }
    }
    console.log(answers);
    let param = {question_id: question_id_list, user_id: id, current_level: max_lvl, answers: answers, minute: minute};
    createRequest('POST', '/play', 'json', param, (result) => {
        if (result) {
            if(result.status) {
                $('#result_score').html(result.total_score);
                $('#result_right').html(result.total_right);
                let wrong = result.wrong_answer;
                let container = $('#result_wrong');
                let i = 1;
                wrong.forEach((item) => {
                    let temp = `
                        <div class="card">
                            <div class="card-header" id="header-wrong${i}">
                                <h5 class="mb-0">
                                    <button class="btn btn-info" data-toggle="collapse" data-target="#wrong${i}" aria-expanded="true" aria-controls="wrong${i}">
                                    Wrong answer #${i}
                                    </button>
                                </h5>
                            </div>
                            <div id="wrong${i}" class="collapse" aria-labelledby="header-wrong${i}" data-parent="#result_wrong">
                                <div class="card-body">
                                    ${item.content}
                                </div>
                            </div>
                        </div>
                    `;
                    i++;
                    container.append(temp);
                });
                if(result.leveled_up) {
                    $('#leveledUp').css('display', '');
                    $('#before_lv').html(max_lvl);
                    $('#after_lv').html(parseInt(max_lvl)+1);
                }
                if(result.level_cap) {
                    $('#level_cap').css('display', '');
                }
                $('#resultModal').modal({backdrop: 'static', keyboard: false});
                $('#resultModal').modal('show');
            }
        }
    });
}

$(document).ready(function() {
    $('body').css('background', "url('/compress/dashboard_bg.png') no-repeat center center fixed");
    $('body').css('-webkit-background-size', "cover");
    $('body').css('-moz-background-size', "cover");
    $('body').css('-o-background-size', "cover");
    $('body').css('background-size', "cover");
    $('body').css('overflow-x', 'hidden');
    
    $('[data-toggle="popover"]').popover({
        placement: function(context, source) {
            let temp = $(source).closest('li').css('justify-content');
            if (temp == 'flex-start') return 'right';
            else if (temp == 'flex-end') return 'left';

            return 'top';
        },
        trigger: 'hover'
    });

    function updatePrevNextBtn() {
        if (curr_page == 1) {
            $('#prev').css('cursor', 'not-allowed');
            $('#prev').attr('disabled', 'disabled');
        } else {
            $('#prev').css('cursor', 'pointer');
            $('#prev').removeAttr('disabled');
        }
        if (curr_page * 5 >= max_lvl) {
            $('#next').css('cursor', 'not-allowed');
            $('#next').attr('disabled', 'disabled');
        } else {
            $('#next').css('cursor', 'pointer');
            $('#next').removeAttr('disabled');
        }
    }

    function updateList(start, newElement) {
        createRequest('GET', `/levels?id=${id}&start=${start}`, 'json', null, (result) => {
            result = result.data;
            let i = 0;
            result.forEach((e) => {
                let item = $(`#item-${i}`);
                item.css('display', '');
                item.find('div').attr('title', e.name);
                item.find('div').attr('data-original-title', e.name);
                item.find('div').attr('data-content', e.description);
                item.find('img').attr('src', `planet/planet-${e.number}.png`);
                item.attr('planet', e.level_id);
                item.attr('planet-name', e.name);
                i++;
            });
            
            if (i != 5) {
                for (; i < 5; i++) {
                    let item = $(`#item-${i}`);
                    item.css('display', 'none');
                }
            }

            if (newElement) {
                prev_selected = $('.active-pages');
                prev_selected.toggleClass('active-pages');
                newElement.toggleClass('active-pages');
            }
        });
    }

    let count = max_lvl/5;
    let pages = $('.pages');
    for (let i = 0, j = 0; i <= count; i++) {
        let item = $('<button></button>');
        item.html((i+1));
        item.attr('index', j);
        j += 5;
        pages.append(item);
        if (i == 0) item.addClass('active-pages');
    }

    updateList(0, $('.active-pages'));
    
    updatePrevNextBtn();

    $('button').on('click', function(e) {
        let elem = $(this);
        if (elem.attr('index')) {
            let idx = elem.attr('index');
            if (idx%5 == 0) {
                curr_page = (idx/5)+1;
            } else {
                curr_page = Math.ceil(idx/5);
            }

            updateList(idx, elem);
            updatePrevNextBtn();
        } else if (elem.attr('id') === 'prev') {
            updateList((curr_page-2)*5, $(`button[index="${(curr_page-2)*5}"]`));
            curr_page--;
            updatePrevNextBtn();
        } else if (elem.attr('id') === 'next') {
            updateList(curr_page*5, $(`button[index="${curr_page*5}"]`));
            curr_page++;
            updatePrevNextBtn();
        }
    });

    $('.planet').click( function(e) {
        $('square-loader').removeClass('dis-none');
        let planet_name = $(this).attr('planet-name');
        let param = {level_id: $(this).attr('planet')};
        createRequest('PUT', '/play', 'json', param, function(result) {
            if(result) {
                $('#ingame #title').html(planet_name);
                let root = $('#ingame .ingame-container .row');
                question_id_list = [];
                for (let i = 1; i <= 10; i++) {
                    let item = result[(i-1)];
                    let answers = item.choices.split('|');
                    question_id_list.push(item.question_id);
                    let container = new ShowHideElement();
                    container.setAttribute('anim-speed', 'fast');
                    container.setAttribute('id', `q${i}`);
                    container.classList.add('col-9');
                    container.classList.add('content');
                    let containerHTML = `
                        <div class="row" id="question-container">
                            ${item.content}
                        </div>
                        <div class="row" id="answer-container">
                            <div class="col-6 col-auto">
                                <button class="col" q_id="${item.question_id}" idx="1">A. ${answers[0]}</button>
                                <button class="col" q_id="${item.question_id}" idx="2">B. ${answers[1]}</button>
                            </div>
                            <div class="col-6 col-auto">
                                <button class="col" q_id="${item.question_id}" idx="3">C. ${answers[2]}</button>
                                <button class="col" q_id="${item.question_id}" idx="4">D. ${answers[3]}</button>
                            </div>
                        </div>
                    `;
                    container.innerHTML = containerHTML;
                    root.append(container);
                }
                document.querySelector('#ingame').showElement();
                document.querySelector('#q1').showElement();
                updateCurrQuestion(1);

                $('#answer-container button').bind('click hover', function(e) {
                    console.log('a');
                    let root = $(this).parent().parent();
                    let temp = root.children('div');
                    temp.children('button').removeClass('answer-active-btn');
                    $(this).addClass('answer-active-btn');
                });
                
                setTimeout(function(e) {
                    $('square-loader').addClass('dis-none');
                    startTimer(600, document.querySelector('#timer'));
                }, 1000);
            }
        });
    });

    $('#question-list button').on('click', function(e) {
        q_number = $(this).attr('q-number');
        if (q_number == curr_question) return;
        updateCurrQuestion(q_number);
    });

    $('#prev-btn').on('click', (e) => {
        prevButton();
    });
    
    $('#next-btn').on('click', (e) => {
        nextButton();
    });

    $('#submit-btn').on('click', (e) => {
        clearInterval(timer_interval);
        endGame();
    });

    $('#result_close').on('click', (e) => {
        window.location.reload();
    });
});

$('.tilt').UniversalTilt({
    scale:1.3,
    reset: true,
    max: 40,
    reverse: true,
    easing: 'cubic-bezier(.03,.98,.52,.99)',
    perspective: 1500,
});