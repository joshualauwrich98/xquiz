function loadErrorModal(data) {
    let container = $('.modal-body');
    container.empty();
    $(data).each((index,element) => {
        container.append(`<div>${element.msg}</div>`);
    });
    
    $('#error-modal').modal('show');
}

$(document).ready(() => {
    //LAYOUT
    $('.limiter').removeClass('dis-none');
    $('.parallax-inner').addClass('popup-show-signin');
    $('form').focusout();

    //UTILITIES
    $("#activation-code").forceNumeric();

    //PARALLAX
    $("body").mousemove((e) => {
        var a, relX, relY;

        a = $(".signin-img-bg");
        relX = e.pageX - a.offset().left;
        relY = e.pageY - a.offset().top;

        TweenMax.to('.signin-img-bg', 1, {
            x: (relX - a.width() / 2) / a.width() * -65,
            y: (relY - a.height() / 2) / a.height() * -65
        });

        a = $(".signup-img-bg");
        relX = e.pageX - a.offset().left;
        relY = e.pageY - a.offset().top;

        TweenMax.to('.signup-img-bg', 1, {
            x: (relX - a.width() / 2) / a.width() * -70,
            y: (relY - a.height() / 2) / a.height() * -70
        });
    });

    //ON CLICK
    $('#signin-forgot-pass').on('click', (e) => {
        document.querySelector('#forgot-pass-email-cont').showElement();
    });
    $('#refresh-sign-in').on('click', (e) => {
        document.querySelector('#signin-cont').showElement();
    });
    $('#sign-up-link').on('click', (e) => {
        document.querySelector('#signup').showElement();
    });
    $('#sign-in-link').on('click', (e) => {
        window.location.reload();
    });
    $('#registration-go-signin').on('click', (e) => {
        window.location.href = '/auth';
    });
    $('#forgot-pass-cancel').on('click', (e) => {
        window.location.href = '/auth';
    });
    $('#forgot-pass-email-cancel').on('click', (e) => {
        window.location.href = '/auth';
    });
    $('#forgot-pass-password-cancel').on('click', (e) => {
        window.location.href = '/auth';
    });

    //ON BLUR LOAD
    $('#signup-form input').bind('blur load', function (e) {
        if ($(this).val()) {
            $(this).siblings(".label-signup-input").text("");
        } else {
            $(this).siblings(".label-signup-input").text($(this).attr('label-name'));
        }
    });

    //ON FOCUS
    $('#signup-form input').on('focus', function (e) {
        $(this).siblings(".label-signup-input").text($(this).attr('label-name'));
    })

    //KEYUP CHECKING
    $('.signup input[name="password_signup"]').bind('keyup change', function (e) {
        let pass = $('input[name="re_password_signup"]');
        if (pass.val() != $(this).val()){
            $(this)[0].setCustomValidity('Confirmation field must be the same with password field');
            pass[0].setCustomValidity('Must be the same with password field');
        } else {
            pass[0].setCustomValidity('');
            $(this)[0].setCustomValidity('');
        }
    });

    $('input[name="re_password_signup"]').bind('keyup change', function (e) {
        let pass = $('.signup input[name="password_signup"]');
        if (pass.val() != $(this).val()){
            $(this)[0].setCustomValidity('Must be the same with password field');
            pass[0].setCustomValidity('Confirmation field must be the same with password field');
        } else {
            pass[0].setCustomValidity('');
            $(this)[0].setCustomValidity('');
        }
    });

    $('.signup input[name="email_signup"]').bind('keyup change', function(e) {
        let obj = {email_signup : $(this).val()};
        createRequest('POST', '/check/email', 'json', obj, (result) => {
            if (result.status) {
                $('.signup input[name="email_signup"]')[0].setCustomValidity('');
                $('#email-check i').removeClass('fa-times-circle');
                $('#email-check i').removeClass('text-red');
                $('#email-check i').addClass('fa-check-circle');
                $('#email-check i').addClass('text-green');
            } else {
                $('.signup input[name="email_signup"]')[0].setCustomValidity('Email already exist');
                $('#email-check i').removeClass('fa-check-circle');
                $('#email-check i').removeClass('text-green');
                $('#email-check i').addClass('fa-times-circle');
                $('#email-check i').addClass('text-red');
            }
        });
    });

    $('input[name="username"]').bind('keyup change', function(e) {
        let obj = {username : $(this).val()};
        createRequest('POST', '/check/username', 'json', obj, (result) => {
            if (result.status) {
                $('input[name="username"]')[0].setCustomValidity('');
                $('#username-check i').removeClass('fa-times-circle');
                $('#username-check i').removeClass('text-red');
                $('#username-check i').addClass('fa-check-circle');
                $('#username-check i').addClass('text-green');
            } else {
                $('input[name="username"]')[0].setCustomValidity('Username arleady taken');
                $('#username-check i').removeClass('fa-check-circle');
                $('#username-check i').removeClass('text-green');
                $('#username-check i').addClass('fa-times-circle');
                $('#username-check i').addClass('text-red');
            }
        });
    });

    //SUBMIT FORM (trying to do it with jquery AJAX than manual HTTP Request)
    $("#signup-form").submit(function (e) {
        e.preventDefault(); // avoid to execute the actual submit of the form.

        var form = $(this);
        var url ='/auth';

        $.ajax({
            type: "PUT",
            url: url,
            dataType: 'json',
            data: form.serialize(), // serializes the form's elements.
        }).done((data) => {
            if (!data.status) {
                loadErrorModal(data.data);
            } else {
                window.location.href = "/confirmation?token_id="+data.token_id;
            }
        });
    });

    $("#signin-form").submit(function (e) {
        e.preventDefault();

        var form = $(this);
        var url = '/auth';
        let email = $('#email').val();
        let password = $('#password').val();

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: form.serialize(), 
        }).done((data) => {
            if (!data.status) {
                loadErrorModal(data.data);
            } else {
                window.location.href = "/dashboard";
            }
        });
    });

    $("#activation-form").submit((e) => {
        e.preventDefault();

        let token_id = $.urlParam('token_id');
        let token_val = $('#activation-code').val();
        
        $.ajax({
            type: "POST",
            url: '/confirmation',
            dataType: 'json',
            data: {token_id: token_id, token_val: token_val},
        }).done((data) => {
            if (!data.status) {
                let temp = new Array();
                temp.push({msg: data.data});
                loadErrorModal(temp);
            } else {
                document.querySelector('#registration-cont').showElement();
            }
        });
    });

    $("#request-reset-pass-form").submit(function (e) {
        e.preventDefault();

        var form = $(this);
        var url = '/reset-password';
        
        $.ajax({
            type: "PUT",
            url: url,
            dataType: 'json',
            data: form.serialize(),
        }).done((data) => {
            if (!data.status) {
                loadErrorModal(data.data);
            } else {
                document.querySelector('#forgot-pass-email-succ-cont').showElement();
            }
        });
    });

    $("#reset-pass-form").submit((e) => {
        e.preventDefault();

        var url = '/reset-password';
        let pass = $('#forgot_pass_password').val();
        let answer = $('#reset_pass_security_ans').val();
        
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: {forgot_pass_password: pass, reset_pass_security_ans: answer, token_id: $.urlParam('token_id'), token_val: $.urlParam('token_val')},
        }).done((data) => {
            if (!data.status) {
                loadErrorModal({msg: data.data});
            } else {
                document.querySelector('#forgot-pass-password-succ-cont').showElement();
            }
        });
    });
});