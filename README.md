# ![XQuiz Logo](http://xquiz.online/favicon.ico "XQuiz Logo")XQuiz

Xquiz is online education games that give the users trivia question to answer with level based game and multiplayer ranking score.

## Live Demo
To see our live demo application, please visit [http://xquiz.online/](http://xquiz.online/ "XQuiz")

## Getting Started

Clone the repository using https
```bash
$ git clone https://gitlab.com/anchovy-studios/xquiz.git
```
or using ssh
```bash
$ git clone git@gitlab.com:anchovy-studios/xquiz.git
```

### Prerequisites

Make sure you have already have `npm` installed on your machine.

### Installing

1. Install the required libraries and node module
    To install the required package make sure you have ```npm``` installed on your machine. Go to the project root and type in below command:
    ```bash
    $ npm install
    ```
    This will automatically installed the required node module package.
    
2. Add the .env files **(IMPORTANT!)**
    This is very important step. Make sure you carefully and completely do this step or the project won't work on your machine.
    - Rename file ```.env-dev``` in the root directory into ```.env```. You can also copy the file and paste it with the name ```.env``` so you still have the original ```.env-dev``` file on your machine.
    - Fill all the required value for environment variables. This is the example.
        ```.env
        # Set up your port
        PORT=8000
        # Set up current work mode (development or production)
        MODE=development
        # Set up the database you will use (sqlite3 or mysql)
        DATABASE=sqlite3
        
        # Set up your database (only for MySQL database)
        DB_HOST=localhost
        DB_USER=db_user
        DB_PASSWORD=db_password
        DB_DATABASE=db_database_name
        
        # Set up your database (only for SQLite3 database)
        DB_NAME=database.db
        
        # Set up SMTP mail account
        SMTP_HOST=yourhost.com
        SMTP_PORT=465
        SMTP_SECURE=true
        SMTP_USER=user@yourhost.com
        SMTP_PASSWORD=1234567890
    ##### Note:
    - The `database` variable is used to tell which database will be use for this application. If `sqlite3` is selected, you ***must*** fill in the required variable for `sqlite3` database, and the same goes for `mysql`.
    - The `mode` variable is used to tell the current mode in your machine. Some features won't be used in `development` mode such as email confirmation, email notification, and forgot password email token. This is to avoid email spam while in `development` mode.
    

## Running the tests

Launch the application by using the command:
```
$ node app.js
```

##### Note:
There is a log folder created in the root directory of your project. You can see error report there or simply see the information that is going in the server.

## Node Package
In this project, we use the following node module package:



| Name        | Link           |
|--- | --- |
| express      | [https://www.npmjs.com/package/express](https://www.npmjs.com/package/express)|
| express-hbs      | [https://www.npmjs.com/package/express-hbs](https://www.npmjs.com/package/express-hbs)      |
| body-parser | [https://www.npmjs.com/package/body-parser](https://www.npmjs.com/package/body-parser)      |
| aes-js | [https://www.npmjs.com/package/aes-js](https://www.npmjs.com/package/aes-js)    |
| pbkdf2 | [https://www.npmjs.com/package/pbkdf2](https://www.npmjs.com/package/pbkdf2)      |
| sqlite3 | [https://www.npmjs.com/package/sqlite3](https://www.npmjs.com/package/sqlite3)      |
| mysql2 | [https://www.npmjs.com/package/mysql2](https://www.npmjs.com/package/mysql2)      |
| express-validator | [https://www.npmjs.com/package/express-validator](https://www.npmjs.com/package/express-validator)     |
| connect-session-sequelize | [https://www.npmjs.com/package/connect-session-sequelize](https://www.npmjs.com/package/connect-session-sequelize)     |
| passport      | [https://www.npmjs.com/package/passport](https://www.npmjs.com/package/passport) |
| randomatic      | [https://www.npmjs.com/package/randomatic](https://www.npmjs.com/package/randomatic) |
| nodemailer      | [https://www.npmjs.com/package/nodemailer](https://www.npmjs.com/package/nodemailer) |
| sequelize      | [https://www.npmjs.com/package/sequelize](https://www.npmjs.com/package/sequelize) |
| dotenv      | [https://www.npmjs.com/package/dotenv](https://www.npmjs.com/package/doten) |
| express-fileupload      | [https://www.npmjs.com/package/express-fileupload](https://www.npmjs.com/package/express-fileupload) |
| serve-favicon | [https://www.npmjs.com/package/serve-favicon](https://www.npmjs.com/package/serve-favicon) |

## Contributing

Until now this is our contributor for this project:
1. Joshua Lauwrich Nandy ([https://gitlab.com/joshualauwrich98](https://gitlab.com/joshualauwrich98))
2. Timothy Lawrence ([https://gitlab.com/timothy.lawrence](https://gitlab.com/timothy.lawrence))

Want to be contributor? Submit an issue and we will go through from that.

## Versioning

Current version is `v1.5`.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

Xquiz is online education games that give the users trivia question to answer with level based game and multiplayer ranking score.

## Live Demo
To see our live demo application, please visit [http://xquiz.online/](http://xquiz.online/ "XQuiz")

## Getting Started

Clone the repository using https
```bash
$ git clone https://gitlab.com/anchovy-studios/xquiz.git
```
or using ssh
```bash
$ git clone git@gitlab.com:anchovy-studios/xquiz.git
```

### Prerequisites

Make sure you have already have `npm` installed on your machine.

### Installing

1. Install the required libraries and node module
    To install the required package make sure you have ```npm``` installed on your machine. Go to the project root and type in below command:
    ```bash
    $ npm install
    ```
    This will automatically installed the required node module package.
    
2. Add the .env files **(IMPORTANT!)**
    This is very important step. Make sure you carefully and completely do this step or the project won't work on your machine.
    - Rename file ```.env-dev``` in the root directory into ```.env```. You can also copy the file and paste it with the name ```.env``` so you still have the original ```.env-dev``` file on your machine.
    - Fill all the required value for environment variables. This is the example.
        ```.env
        # Set up your port
        PORT=8000
        # Set up current work mode (development or production)
        MODE=development
        # Set up the database you will use (sqlite3 or mysql)
        DATABASE=sqlite3
        
        # Set up your database (only for MySQL database)
        DB_HOST=localhost
        DB_USER=db_user
        DB_PASSWORD=db_password
        DB_DATABASE=db_database_name
        
        # Set up your database (only for SQLite3 database)
        DB_NAME=database.db
        
        # Set up SMTP mail account
        SMTP_HOST=yourhost.com
        SMTP_PORT=465
        SMTP_SECURE=true
        SMTP_USER=user@yourhost.com
        SMTP_PASSWORD=1234567890
    ##### Note:
    - The `database` variable is used to tell which database will be use for this application. If `sqlite3` is selected, you ***must*** fill in the required variable for `sqlite3` database, and the same goes for `mysql`.
    - The `mode` variable is used to tell the current mode in your machine. Some features won't be used in `development` mode such as email confirmation, email notification, and forgot password email token. This is to avoid email spam while in `development` mode.

3. There is sample database data in `sample.txt` that can be used. Execute it in your database.
    

## Running the tests

Launch the application by using the command:
```
$ node app.js
```

##### Note:
There is a log folder created in the root directory of your project. You can see error report there or simply see the information that is going in the server.

## Node Package
In this project, we use the following node module package:



| Name        | Link           |
|--- | --- |
| express      | [https://www.npmjs.com/package/express](https://www.npmjs.com/package/express)|
| express-hbs      | [https://www.npmjs.com/package/express-hbs](https://www.npmjs.com/package/express-hbs)      |
| body-parser | [https://www.npmjs.com/package/body-parser](https://www.npmjs.com/package/body-parser)      |
| aes-js | [https://www.npmjs.com/package/aes-js](https://www.npmjs.com/package/aes-js)    |
| pbkdf2 | [https://www.npmjs.com/package/pbkdf2](https://www.npmjs.com/package/pbkdf2)      |
| sqlite3 | [https://www.npmjs.com/package/sqlite3](https://www.npmjs.com/package/sqlite3)      |
| mysql2 | [https://www.npmjs.com/package/mysql2](https://www.npmjs.com/package/mysql2)      |
| express-validator | [https://www.npmjs.com/package/express-validator](https://www.npmjs.com/package/express-validator)     |
| connect-session-sequelize | [https://www.npmjs.com/package/connect-session-sequelize](https://www.npmjs.com/package/connect-session-sequelize)     |
| passport      | [https://www.npmjs.com/package/passport](https://www.npmjs.com/package/passport) |
| randomatic      | [https://www.npmjs.com/package/randomatic](https://www.npmjs.com/package/randomatic) |
| nodemailer      | [https://www.npmjs.com/package/nodemailer](https://www.npmjs.com/package/nodemailer) |
| sequelize      | [https://www.npmjs.com/package/sequelize](https://www.npmjs.com/package/sequelize) |
| dotenv      | [https://www.npmjs.com/package/dotenv](https://www.npmjs.com/package/doten) |
| express-fileupload      | [https://www.npmjs.com/package/express-fileupload](https://www.npmjs.com/package/express-fileupload) |
| serve-favicon | [https://www.npmjs.com/package/serve-favicon](https://www.npmjs.com/package/serve-favicon) |

## Contributing

Until now this is our contributor for this project:
1. Joshua Lauwrich Nandy ([https://gitlab.com/joshualauwrich98](https://gitlab.com/joshualauwrich98))
2. Timothy Lawrence ([https://gitlab.com/timothy.lawrence](https://gitlab.com/timothy.lawrence))

Want to be contributor? Submit an issue and we will go through from that.

## Versioning

Current version is `v1.5`.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
